﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PetaPoco;
using TestMultiInsert.Models;
using System.IO;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using Winista.Mime;
using System.Reflection;
using TestMultiInsert.Model;

namespace TestMultiInsert
{
    public class Oracle
    {
        public int ID { get; set; }
        public string NAME { get; set; }
    }
    public class SqlEntity
    {
        public bool Id { get; set; }
        public string Name { get; set; }
    }
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var strs = new string[] { "2", "4", "6"};
            var ints = new int[] { 1, 2, 3 };
            var obj = new object[1];
            obj[0] = strs;
            Sql sql2 = Sql.Builder.Where("ID in (@str)",new {str= strs });
            var check = sql2.Arguments;
            using (var context = new DataTestContext())
            {
               
                var testinsert = new STGDOC() { NAME = "dac1" };
                // var ad = context.Insert(testinsert);
                try
                {
                    Sql sql = Sql.Builder.From("Account");
                    Sql sql1 = Sql.Builder.Where("ID=@0",1);
                    var arrs = context.Fetch<Account>(sql);
                    var list = context.Exists<Account>(sql1);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var fileInfo = new FileInfo(@"F:\test.docx");
            string strpath = Path.Combine(@"F:\testfile\", txtfile.Text);
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lblcontenttype.Text = getMimeFromFile(openFileDialog1.FileName);
            }

        }
        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static System.UInt32 FindMimeFromData(
        System.UInt32 pBC,
        [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
        [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
        System.UInt32 cbSize,
        [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
        System.UInt32 dwMimeFlags,
        out System.UInt32 ppwzMimeOut,
        System.UInt32 dwReserverd
    );

        public static string getMimeFrom256(string filename)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException(filename + " not found");

            byte[] buffer = new byte[256];
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                if (fs.Length >= 256)
                    fs.Read(buffer, 0, 256);
                else
                    fs.Read(buffer, 0, (int)fs.Length);
            }
            try
            {
                System.UInt32 mimetype;
                FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);
                System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                string mime = Marshal.PtrToStringUni(mimeTypePtr);
                Marshal.FreeCoTaskMem(mimeTypePtr);
                return mime;
            }
            catch (Exception e)
            {
                return "unknown/unknown";
            }
        }
        public static string getMimeFromFile(string filename)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException(filename + " not found");

            MimeTypes g_MimeTypes = new MimeTypes("mime-types.xml");
            sbyte[] fileData = null;

            using (System.IO.FileStream srcFile =
                new System.IO.FileStream(filename, System.IO.FileMode.Open))
            {
                byte[] buffer = new byte[256];
                // byte[] data = new byte[srcFile.Length];
                //srcFile.Read(data, 0, (Int32)srcFile.Length);
                if (srcFile.Length >= 256)
                    srcFile.Read(buffer, 0, 256);
                else
                    srcFile.Read(buffer, 0, (int)srcFile.Length);
                fileData = Winista.Mime.SupportUtil.ToSByteArray(buffer);
            }
            MimeType oMimeType = g_MimeTypes.GetMimeType(fileData);
            if (oMimeType == null)
                return null;
            return oMimeType.Name;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(RandomString(int.Parse(txtfile.Text)));
        }
        public string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        
        private void button4_Click(object sender, EventArgs e)
        {
            Oracle oOracle = new Oracle();
            var sql = new SqlEntity
            {
                Id = true,
                Name = "test"
            };
           oOracle = ToolConvert.CopySqlToOracle<Oracle>(oOracle, sql);
        }
     
    }
}
