﻿using ShopRetailCore.Interfaces;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Interfaces
{
    public interface IImport : IRepository<Import>
    {
    }
}
