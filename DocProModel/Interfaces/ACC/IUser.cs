﻿using ShopRetailCore.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Interfaces
{
    public interface IUser : IRepository<User>
    {
        bool ValidateLogin(string username, string password, out User user);
    }
}
