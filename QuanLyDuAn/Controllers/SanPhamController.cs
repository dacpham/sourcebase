﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Dac;
using ShopDetail.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailModel.Params;
using static ShopRetailReponsitoty.Enum.Enum;
using ShopRetailModel.Models;

namespace ShopAdmin.Controllers
{
    public class SanPhamController : BaseController
    {
        public SanPhamController(IUser resUser, ICategory resCategory, IExport resExport,
            IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest,
            Import resImport, IOrder resOrder, IOrderDetail resOrderDetail,
            IProduct resProduct, IProductViewed resProductViewed,
            ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
            
        }

        public ActionResult Index(int id = 0)
        {
            //var productpr = DACS.Bind<ProductParam>(DATA);
            //var products = _resProduct.Search(productpr, Paging);
            //var omdProduct = new ContentCategroryModel();
            //omdProduct.Contents = products;
            //omdProduct.Categories = _resCategory.GetListByFields("ID", products.Select(t => t.IDCategory).ToArray());
            //omdProduct.GetContentDetail();
            //ViewBag.ProductParam = productpr;
            //return GetCustResultOrView("Index", omdProduct.ContentViews);
            return null;
        }
        public ActionResult Sua(long id = 0)
        {
            var product = _resProduct.GetById(id) ?? new Product();
            var productmodel = new ProductModel();
            if(product.ID ==0)
            {
                return RedirectToPath("/admin/san-pham.html");
            }
            else
            {
                productmodel = DACS.CopyTo<ProductModel>(product);
                productmodel.Events = _resEvent.GetAll();
                productmodel.Files = _resStgFiles.GetListByField("IDDoc",(int)product.ID);
            }
            ViewBag.Category = DACS.RenderOptions(_resCategory.GetAll(), product.IDCategory, true, "Chọn danh mục sản phẩm");
            return GetCustResultOrView("ChiTiet", productmodel);
        }
        public ActionResult ThemMoi(int id = 0)
        {
            var product =  new ProductModel();
            product.Events = _resEvent.GetAll();
            product.Files = _resStgFiles.GetListByField("IDDoc",0);
            ViewBag.Category = DACS.RenderOptions(_resCategory.GetAll(),0, true, "Chọn danh mục sản phẩm");
            return GetCustResultOrView("ChiTiet", product);
        }
        /// <summary>
        /// Them cac file anh cua san pham
        /// </summary>
        /// <param name="IDDoc"></param>
        private void BindFileRefer(long IDDoc)
        {
            var filepaths = DACS.GetStrings(DATA, "FilePaths");
            var stgfiles = new List<StgFiles>();
            if(filepaths != null && filepaths.Count() >0)
            {
                foreach (var item in filepaths)
                {
                    var stgfile = new StgFiles();
                    stgfile.Created = DateTime.Now;
                    stgfile.CreatedBy = CUser.ID;
                    stgfile.ThumbPath = item;
                    stgfile.ThumbName = GetNameFromPath(item);
                    stgfile.Type = (int)FileType.Hinhanh;
                    stgfile.IDDoc = IDDoc;
                    stgfiles.Add(stgfile);
                }
                _resStgFiles.Deletes("IDDoc",IDDoc);
                _resStgFiles.Inserts(stgfiles);
            }
        }
        public ActionResult CapNhat()
        {
            var product = DACS.Bind<Product>(DATA);
            var avatar = DACS.GetString(DATA, "FilePathProducts");
            var issale =string.IsNullOrEmpty( DACS.GetString(DATA, "IsSale") ) ?false :true;
            var isnew = string.IsNullOrEmpty(DACS.GetString(DATA, "IsNew")) ? false : true;
            if (product.ID == 0) // Insert
            {
                var entity = DACS.BindCreate<Product>(product, CUser.ID);
                entity.ThumbPath = avatar;
                entity.ThumbName = GetNameFromPath(avatar);
                entity.IsSale = issale;
                entity.IsNew = isnew;
                if (_resProduct.Insert(entity))
                {
                    BindFileRefer(entity.ID);
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                var entity = _resProduct.GetById(product.ID);
                //entity = DACS.Bind<Category>(entity, DACS.KeyValue<Product>(product), 0, CUser.ID);
                entity = DACS.Bind<Product>(entity, DATA, 0, CUser.ID);
                entity.ThumbPath = avatar;
                entity.ThumbName = GetNameFromPath(avatar);
                entity.IsSale = issale;
                entity.IsNew = isnew;
                if (_resProduct.Update(entity))
                {
                    BindFileRefer(entity.ID);
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return RedirectToPath(DACS.ReUrl(DACS.admin +"/sanpham.html",""));
        }
        public ActionResult Xoa()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resProduct.GetItems(ids);
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn tử cần xóa không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                if (_resProduct.Deletes(entitys.Select(t=>t.ID).ToArray()) >=0)
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return RedirectToPath( GetReferrerOrDefault("/home.html"));
        }
        //huy xuất bản
        public ActionResult XuatBan()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resProduct.GetItems(ids).ToList();
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn từ không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                try
                {
                    entitys.ForEach(t =>
                    {
                        t.IsPublish = true;
                        t.PublishBy = CUser.ID;
                        t.Published = DateTime.Now;
                        t.BindUpdate<Product>(CUser.ID);
                    });
                }
                catch (Exception ex)
                {
                    Loger.Log(ex.ToString());
                    throw;
                }
              
                if (_resProduct.Updates(entitys, "IsPublish", "PublishBy", "Published", "Updated","UpdatedBy"))
                    SetSuccess(DACS.T("Thao tác xuất bản thành công"));
                else
                {
                    SetError(DACS.T("Thao tác xuất bản không thành công"));
                }
            }
            return RedirectToPath(GetReferrerOrDefault("/home.html"));
        }
        public ActionResult HuyXuatBan()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resProduct.GetItems(ids).ToList();
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn từ không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                entitys.ForEach(t =>
                {
                    t.IsPublish = false;
                    t.PublishBy = CUser.ID;
                    t.Published = DateTime.Now;
                    t.BindUpdate<Product>(CUser.ID);
                });
                if (_resProduct.Updates(entitys))
                    SetSuccess(DACS.T("Thao tác xuất bản thành công"));
                else
                {
                    SetError(DACS.T("Thao tác xuất bản không thành công"));
                }
            }
            return RedirectToPath(GetReferrerOrDefault("/home.html"));
        }
    }
}