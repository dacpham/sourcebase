﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailCore.Params
{
    public abstract class CommonParam
    {
        public string Order { get; set; }
        public abstract string GetQuery();
        public string GetOrder()
        {
            return Order;
        }
    }
}
