using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poco = PetaPoco;
namespace ShopRetailModel.Models
{
    [Poco.TableName("RoleModule")]
    [Poco.PrimaryKey("ID", AutoIncrement = true)]
    [Poco.ExplicitColumns]
    public partial class RoleModule : CreateUpdate
    {
        [Key]
        [Poco.Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ID { get; set; } // int    

        [Poco.Column]
        [Display(Name = "IDRole")]
        public int IDRole { get; set; } // int 

        [Poco.Column]
        [Display(Name = "IDModule")]
        public int IDModule { get; set; }
    }
}
