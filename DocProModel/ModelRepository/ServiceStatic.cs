﻿using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;
using ShopRetailCore.Interfaces;
using System.Data.SqlClient;
using System.Data;

namespace ShopRetailModel.ModelRepository
{
    public static class ServiceStatic
    {
        public static Account GetUserById(object id)
        {
            var db = new PetaPoco.Database("ShopRetailConnectionString");
            return db.SingleOrDefault<Account>(id);
        }
        public static Guest GetGuestById(object id)
        {
            var db = new PetaPoco.Database("ShopRetailConnectionString");
            return db.SingleOrDefault<Guest>(id);
        }
        public static bool IsExistsModule(int iduser,int[]modules)
        {
            var result = new SqlParameter("@@result", SqlDbType.Bit);
            result.Direction = ParameterDirection.Output;
            var db = new PetaPoco.Database("ShopRetailConnectionString");
            db.Execute("; EXEC SP_CheckGrantByUserModule @0,@1,@2 OUTPUT", iduser,string.Join(",",modules),result);
            return (bool)result.Value;
        }
    }
}
