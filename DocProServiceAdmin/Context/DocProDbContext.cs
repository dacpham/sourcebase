﻿using ShopRetailCore.Interfaces;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailServiceAdmin.Context
{
    public class DocProDbContext : IDbContext
    {
        private bool disposed = false;
        private Database _database;
        public DocProDbContext()
        {

        }
        protected void SetDatabase(string connectionStringName)
        {
            _database = new Database(connectionStringName);
        }
        public Database DbInstance
        {
            get
            {
                return _database;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _database.Dispose();
                }
                disposed = true;
            }
        }
    }
}
