﻿using ShopRetail.Models;
using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using ShopRetailModel.Interfaces;

namespace ShopRetail.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, IImport resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IImportDetail resImportDetail, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resImportDetail, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
        }
    }
}