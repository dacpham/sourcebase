﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DocProResumable
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private const string _WebApiPrefix = "api";
        private static readonly string _WebApiExecutionPath = String.Format("~/{0}", _WebApiPrefix); 
        protected void Application_Start()
        { 
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
