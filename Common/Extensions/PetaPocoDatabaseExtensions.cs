﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public static class PetaPocoDatabaseExtensions
    {
        public static void BulkInsert<T>(this Database db, DataTable dt) where T : class
        {

            db.OpenSharedConnection();
            try
            {

                SqlBulkCopy objbulk = new SqlBulkCopy("");
                //assigning Destination table name  
                objbulk.DestinationTableName = db.GetTableName<T>();
                //Mapping Table column  
                //inserting bulk Records into DataBase   
                objbulk.WriteToServer(dt);
            }
            finally
            {
                db.CloseSharedConnection();
            }

        }
    }
}
