using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poco = PetaPoco;
namespace ShopRetailModel.Models
{
    [Poco.TableName("AccountRole")]
    [Poco.PrimaryKey("ID", AutoIncrement = true)]
    [Poco.ExplicitColumns]
    public partial class AccountRole : CreateUpdate
    {
        [Key]
        [Poco.Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ID { get; set; } // int    

        [Poco.Column]
        [Display(Name = "IDRole")]
        public int IDRole { get; set; } // int 

        [Poco.Column]
        [Display(Name = "IDAccount")]
        public int IDAccount { get; set; }
    }
}
