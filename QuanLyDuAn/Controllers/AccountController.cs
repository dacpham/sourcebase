﻿using QuanLyDuAn.Models;
using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using Common.Dac;
using QuanLyDuAn.Customs.Enum;
using ShopRetailModel.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Params;
using QuanLyDuAn.Customs.Params;
using QuanLyDuAn.Customs.Utility;
using static QuanLyDuAn.Customs.Enum.CusEnum;

namespace ShopAdmin.Controllers
{
    public class AccountController : BaseController
    {
        private string defaultPath = "/account.html";
        public AccountController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles, IAccount resAccount, IDept resDept, IPosition resPosition, IModule resModule, IRole resRole, IRoleModule resRoleModule, IAccountRole resAccountRole) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles, resAccount, resDept, resPosition, resModule, resRole, resRoleModule, resAccountRole)
        {
        }
        #region Login
        [HttpGet]
        public ActionResult Login()
        {
            var crRequest = "/home.html";
            SetTitle(DACS.T("Đăng nhập hệ thống"));
            return GetResultOrView("Login", crRequest);
        }
        [HttpPost]
        public ActionResult Login(int id = 0)
        {
            var account = new Account();
            var redirectPath = DACS.Base64Decode(DACS.GetString(DATA, "RedirectPath"));
            var entity = DACS.Bind<Account>(DATA);
            if (_resAccount.ValidateLogin(entity.Username, entity.Password, out account))
            {
                DacAuthen.SetLoginUser(account, true, HttpContext.Request);
                SetSuccess(DACS.T("Đăng nhập thành công"));
                return RedirectToPath(redirectPath);
            }
            SetError(DACS.T("Đăng nhập không thành công"));
            return GetResultOrView("Login");
        }
        [HttpGet]
        public ActionResult LogOut()
        {
            DacAuthen.SetLogOutUser(HttpContext.Request);
            return RedirectToPath("/login/account/login.html");
        }
        //public ActionResult Auth(int id = 0)
        //{
        //    switch (id)
        //    {
        //        case (int)GuestType.Facebook:

        //            Session["CPage"] = GetReferrerOrDefault("home.html");
        //            var face = new FacebookClient();
        //            var loginurl = face.GetLoginUrl(new
        //            {
        //                client_id = GlobalConfig.GetStringSetting("FbAppID"),
        //                client_secrect = GlobalConfig.GetStringSetting("FbAppSecrect"),
        //                redirect_uri = CUtility.GetRedirectUri(Request.Url, "FacebookCallback"),
        //                response_type = "code",
        //                scope = "email"
        //            });
        //            return Redirect(loginurl.AbsoluteUri);

        //        default:
        //            var guest = Utils.Bind<Guest>(DATA);
        //            TempData["UserLogin"] = guest;
        //            if (GuestRepository.ValidateLogin(guest.Username, guest.Password, out guest))
        //            {
        //                if (guest.IsApproved && guest.IsPublished)
        //                {
        //                    AclConfig.SetLogedInGuest(guest, true);
        //                    SetSuccess(DACS.T("Đăng nhập thành công"));
        //                }
        //                else
        //                {
        //                    SetError(DACS.T("Tài khoản chưa được kích hoạt từ email đăng ký"));
        //                }
        //            }
        //            else
        //            {
        //                SetError(DACS.T("Đăng nhập không thành công"));
        //            }
        //            if (HasError)
        //            {

        //                return RedirectToPath("/account/login.html");
        //            }
        //            return GetResultOrRedirectDefault("/home.html");
        //    }
        //}
        #region Facebook
        //public ActionResult FacebookCallback(string code)
        //{
        //    var facebookClient = new FacebookClient();
        //    dynamic result = facebookClient.Post("oauth/access_token", new
        //    {
        //        client_id = DACS.GetStringSetting("FbAppID"),
        //        client_secrect = DACS.GetStringSetting("FbAppSecrect"),
        //        redirect_uri = DACS.GetRedirectUri(Request.Url, "FacebookCallback"),
        //        code = code,
        //    });

        //    var accessToken = result.access_token;
        //    if (!string.IsNullOrEmpty(accessToken))
        //    {
        //        facebookClient.AccessToken = accessToken;
        //        dynamic me = facebookClient.Get("me?fields=first_name,middle_name,last_name,id,email");

        //        var guest = new Guest();
        //        guest.Email = me.email;
        //        guest.PhoneNumber = string.Empty;
        //        guest.UserName = me.first_name;
        //        guest.Code = me.id;
        //        guest.CreateDate = DateTime.Now;
        //        guest.CreatedBy = 0;
        //        guest.TypeGuest = (int)CusEnum.TypeGuest.Facebook;
        //        guest.Avatar = string.Format("https://graph.facebook.com/{0}/picture?type=large", me.id);
        //        if (GuestReponsitory.InsertOrSubmit(guest))
        //        {
        //            DacAuthen.SetLoginGuest(guest, true);
        //            SetSuccess(DACS.T("Đăng nhập thành công"));
        //        }
        //        else
        //        {
        //            SetError(DACS.T("Đăng nhập không thành công,vui lòng đăng nhập lại"));
        //        }
        //    }
        //    return RedirectToPath("/Account/FormWinDowClose.html");
        //}
        //public ActionResult FormWinDowClose()
        //{
        //    return GetCustResultOrView("FormWinDowClose");
        //}
        #endregion

        #endregion

        #region Manager
        public ActionResult Index()
        {
            SetTitle(DACS.T("Danh sách người dùng"));
            var param = DACS.Bind<AccountParam>(DATA);
            var accounts = _resAccount.Search(param, Paging);
            var depts = _resDept.GetListByField("IDChannel", CUser.IDChannel) ?? new List<Dept>();
            var positions = _resPosition.GetListByField("IDChannel", CUser.IDChannel) ?? new List<Position>();
            var positionIds = accounts.Select(n => n.IDPosition > 0);

            var model = new AccountModel()
            {
                Accounts = accounts,
                SearchParam = param,
                Depts = depts,
                Positions = positions,
                Url = DACS.ReUrl("/account.html")
            };
            return GetCustResultOrView(new ViewParam()
            {
                ViewName = "Index",
                ViewNameAjax = "Accounts",
                Data = model
            });
        }


        public ActionResult Create()
        {
            SetTitle(DACS.T("Tạo mới tài khoản"));
            var acc = new Account() { Gender = (int)Gender.Male };
            var depts = _resDept.GetListByField("IDChannel", CUser.IDChannel) ?? new List<Dept>();
            var positions = _resPosition.GetListByField("IDChannel", CUser.IDChannel) ?? new List<Position>();
            var model = new AccountModel()
            {
                Account = acc,
                Depts = depts,
                Positions = positions,
                DicGender = CUtility.EnumToDic<Gender>(),
                DicStatus = CUtility.EnumToDic<AccountStatus>(),
                Url = DACS.ReUrl("/account/save.html")
            };

            return GetDialogResultOrView("Update", model, 800);
        }

        public ActionResult Update(int id = 0)
        {

            var acc = _resAccount.GetById(id);
            if (DACS.IsEmpty(acc))
            {
                SetError(DACS.T("Tài khoản không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }

            SetTitle(DACS.T("Cập nhật tài khoản"));
            var depts = _resDept.GetListByField("IDChannel", CUser.IDChannel) ?? new List<Dept>();
            var positions = _resPosition.GetListByField("IDChannel", CUser.IDChannel) ?? new List<Position>();
            var model = new AccountModel()
            {
                Account = acc,
                Depts = depts,
                Positions = positions,
                DicGender = CUtility.EnumToDic<Gender>(),
                DicStatus = CUtility.EnumToDic<AccountStatus>()
            };
            return GetDialogResultOrView("Update", model, 800);
        }

        public ActionResult Save()
        {
            var account = DACS.Bind<Account>(DATA);
            if (account.ID == 0) // Insert
            {
                var entity = DACS.BindCreate<Account>(account, CUser.ID);
                entity.Password = DACS.Encrypt(entity.Password, true);
                if (_resAccount.Insert(entity))
                {
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                var entity = _resAccount.GetById(account.ID);
                entity = DACS.Bind<Account>(entity, DATA, 0, CUser.ID);
                if (_resAccount.Update(entity))
                {
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return GetResultOrRedirectDefault(DACS.ReUrl(DACS.admin + defaultPath));
        }
        public ActionResult Delete()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resAccount.GetItems(ids).ToList();
            if (Equals(entitys, null))
                SetError(DACS.T("Tài khoản cần xóa không tồn tại, vui lòng kiểm tra lại"));
            else
            {
                if (_resAccount.Deletes(entitys.Select(t => t.ID).ToArray()) >= 0)
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return RedirectToPath(DACS.ReUrl(DACS.admin + defaultPath));
        }
        #endregion
    }
}