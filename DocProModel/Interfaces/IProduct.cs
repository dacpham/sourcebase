﻿using Common.Extensions;
using ShopRetailCore.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Interfaces
{
    public interface IProduct : IRepository<Product>
    {
        List<Product> GetListByCategory(long idcategory,int take=10);
        List<Product> GetListByCategory(long idcategory, Pagination paging);
    }
  
}
