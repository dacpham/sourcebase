﻿using ShopRetailModel.Models;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyDuAn.Models
{
    public class RoleModel
    {
        public List<Role> Roles { get; set; }
        public List<Role> Parents { get; set; }
        public Role Role { get; set; }
        public RoleParam SearchParam { get; set; }
        public string Url { get; set; }
    }
}