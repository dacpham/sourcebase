﻿jQuery(function ($) {
    if ($(window).width() > 999) {
        $('#mainmenu li').hover(function () {
            clearTimeout($.data(this, 'timer'));
            $(this).children('.dropdown-menu').stop(true, true).slideDown(100);
        }, function () {
            $.data(this, 'timer', setTimeout(
                $.proxy(function () {
                    $('.dropdown-menu', this).stop(true, true).fadeOut(10);
                }, this)
                , 100));
        });
        $('#mainmenu li.dropdown > a').click(function () {
            if ($(this).attr("href") != "javascript:void(0);" && $(this).attr("href") != "#") {
                window.location.href = $(this).attr("href");
            }
        });

        $(".owl-carousel-if-1k").owlCarousel({
            pagination: false,
            navigation: false,
        });
    }
    else {
        $('#mainmenu ul.dropdown-menu a').click(function () {
            var ul = $(this).parent().find(".dropdown-menu");
            if (ul.length > 0 && $(this).attr("href") != "javascript:void(0);") {
                ul.toggle();
                return false;
            }
        });
        $(".owl-carousel-if-1k").append("<div class='clearfix'></div>").addClass("owl-carousel-if-1k-mobile");
    }
    $(".owl-carousel-gen-single").owlCarousel({
        pagination: true,
        autoPlay: true,
        singleItem: true,
        stopOnHover: true,
    });
    $(".owl-carousel-gen").owlCarousel({
        pagination: false,
        navigation: false,
    });
    $(".owl-carousel-gen-nav").owlCarousel({
        items: 5,
        pagination: false,
        autoPlay: true,
        stopOnHover: true,
        navigation: true,
        navigationText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
    });

    // js for show list and grid
    $(".fa-btn.fa-th").click(function () {
        var section = $(this).closest(".section");
        section.find(".fa-btn.fa-list").removeClass("active"); $(this).addClass("active"); section.find(".music-wrapper").show(); section.find(".music-list-wrapper").hide();
        return false;
    });
    $(".fa-btn.fa-list").click(function () {
        var section = $(this).closest(".section");
        section.find(".fa-btn.fa-th").removeClass("active"); $(this).addClass("active"); section.find(".music-list-wrapper").show(); section.find(".music-wrapper").hide();
        return false;
    });
    //------------------------------

    $(document).on("click", ".slideDown", function (e) {
        e.preventDefault();
        var data = $(this).getData();
        if ($(data.formid).is(":visible")) {
            $(data.formid).addClass("hidden");
            $(data.divid).addClass("hidden");
            $(data.target).slideUp("slow");
        }
        else {
            $(data.target).find("form").addClass("hidden");
            $(data.formid).removeClass("hidden");
            $(data.divid).removeClass("hidden").fadeIn();;
            var height = jQuery(data.target).outerHeight();
            jQuery(data.target).css("height", height + "px");
           $(data.target).slideDown("slow");
        }
    });
    $(document).on("click", ".tg-hd", function () {
        var main = $(this).siblings(".m-tg-hd");
        var height = main.data("height");
        var crt = main.data("crt");
        var aft = main.data("aft");
        if(main.hasClass('at'))
        {
            main.css("height", 'auto'); 
            main.removeClass('at');
            $(this).html(aft);
        }
        else {
            main.css("height", height);
            main.css("overflow", 'hidden');
            main.addClass('at');
            $(this).html(crt);
        }
    });
    $(function () {
        $(".select2").each(function () {
            $(this).select2({
                placeholder: $(this).attr("placeholder"),
                tags: true
            });
        });
    });

});