﻿using ShopRetailCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;
using ShopRetailServiceAdmin.Interfaces;
using ShopRetailCore.Params;
using Common.Dac;
using Common.Extensions;

namespace ShopRetailCore.Context
{
    public class Repository<T> : Repository, IRepository<T> where T : class, new()
    {
        private string _tableName = null;
        protected string TableName
        {
            get
            {
                try
                {
                    if (_tableName == null)
                    {
                        _tableName = _dbContext.DbInstance.GetTableName<T>();
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }
                return _tableName;
            }
        }

        public Repository(IDbContext dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {
        }

        public int DeleteFlag(Sql sqlQuery)
        {
            return _dbContext.DbInstance.Update(sqlQuery);
        }
        public int DeleteFlags(params object[] ids)
        {
            Sql qrDelete = Sql.Builder.Set("IsDeleted = @0", true).From(TableName).Where("ID in (@0)", ids);
            return Update(qrDelete);
        }
        public int DeleteFlag(T entity)
        {
            try
            {
                return _dbContext.DbInstance.Update(entity, new string[] { "IsDeleted" });
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return 0;
            }
        }

        public int Delete(Sql sqlQuery)
        {
            try
            {
                return _dbContext.DbInstance.Delete(sqlQuery);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return 0;
            }
        }

        public int Deletes(string field, params object[] fields)
        {
            try
            {
                return _dbContext.DbInstance.Delete<T>(string.Format("where {0} IN ( @0 )", field), fields);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return 0;
            }
        }
        public int Deletes(params object[] ids)
        {
            try
            {
                return _dbContext.DbInstance.Delete<T>("Where ID in (@0)", ids);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return 0;
            }
        }

        public T GetById(string id)
        {
            try
            {
                return _dbContext.DbInstance.FirstOrDefault<T>(Sql.Builder.From(TableName).Where("ID = @0", id));
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public T GetById(Guid id)
        {
            try
            {
                return _dbContext.DbInstance.FirstOrDefault<T>(Sql.Builder.From(TableName).Where("ID = @0", id));
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public T GetById(long id)
        {
            try
            {
                return _dbContext.DbInstance.FirstOrDefault<T>(Sql.Builder.From(TableName).Where("ID = @0", id));
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public T GetById(int id)
        {
            try
            {
                return _dbContext.DbInstance.FirstOrDefault<T>(Sql.Builder.From(TableName).Where("ID = @0", id));
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public T GetOne(Sql sqlQuery)
        {
            try
            {
                return _dbContext.DbInstance.SingleOrDefault<T>(sqlQuery);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public T GetOne(object primaryKey)
        {
            try
            {
                return _dbContext.DbInstance.SingleOrDefault<T>(primaryKey);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public T GetOne(string sql, params object[] args)
        {
            try
            {
                return _dbContext.DbInstance.SingleOrDefault<T>(sql, args);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public List<T> GetAll()
        {
            try
            {
                return _dbContext.DbInstance.Fetch<T>(Sql.Builder.Select("*").From(_dbContext.DbInstance.GetTableName<T>()));
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }
        public List<T> GetAll(Pagination paging)
        {
            try
            {
                long page = paging.PageIndex;
                long itemsPerPage = paging.PageSize;
                Sql sqlCount = Sql.Builder
                    .Select("SELECT COUNT(1)")
                    .From(_tableName);
                Sql sqlPage = Sql.Builder
                    .Select("*")
                    .From(_tableName);
                var result = _dbContext.DbInstance.Page<T>(page, itemsPerPage, sqlCount, sqlPage);
                paging.RowCount = (int)result.TotalItems;
                return result.Items;
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public List<T> GetItems(Sql sqlQuery)
        {
            try
            {
                return _dbContext.DbInstance.Fetch<T>(sqlQuery);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public List<T> GetItems(string sql, params object[] args)
        {
            try
            {
                return _dbContext.DbInstance.Query<T>(sql, args).ToList();
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public Page<T> Page(Sql sqlQuery, int page, int rowLimit)
        {
            try
            {
                return _dbContext.DbInstance.Page<T>(page, rowLimit, sqlQuery);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public string GetTableName()
        {
            try
            {
                return _dbContext.DbInstance.GetTableName<T>();
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return string.Empty;
            }
        }

        public int Update(string sql, params object[] args)
        {
            try
            {
                return _dbContext.DbInstance.Update<T>(sql, args);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return 0;
            }
        }

        public int Update(Sql sqlQuery)
        {
            try
            {
                return _dbContext.DbInstance.Update<T>(sqlQuery);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return 0;
            }
        }

        public bool Update(T poco, params string[] columns)
        {
            try
            {
                return columns.Length > 0 ? _dbContext.DbInstance.Update(poco, columns) > 0 : _dbContext.DbInstance.Update(poco) > 0;
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }

        public bool Insert(T poco)
        {
            try
            {
                return DACS.IsNotEmpty(_dbContext.DbInstance.Insert(poco));
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }

        }
        public object Insert(string tableName, T poco)
        {
            try
            {
                return _dbContext.DbInstance.Insert(tableName, poco);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        public object Insert(string tableName, string primaryKeyName, object poco, bool autoIncrement = true)
        {
            try
            {
                return _dbContext.DbInstance.Insert(tableName, primaryKeyName, autoIncrement, poco);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }
        public bool Inserts(List<T> pocos)
        {
            try
            {
                bool result = true;
                _unitOfWork.BeginTransaction();
                if (DACS.IsEmpty<T>(pocos))
                    return false;
                foreach (var item in pocos)
                {
                    result= result && !DACS.IsEmpty(Insert(item));
                }
                if (result)
                    _unitOfWork.CommitTransaction();
                return result;
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }

        }

        public void Save(string tableName, string primaryKey, T poco)
        {
            try
            {
                _dbContext.DbInstance.Save(tableName, primaryKey, poco);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
            }
        }
        public void Save(T poco)
        {
            try
            {
                _dbContext.DbInstance.Save(poco);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
            }
        }

        public bool Exists(object primaryKey)
        {
            try
            {
                return _dbContext.DbInstance.Exists<T>(primaryKey);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }

        protected bool Exists(string sqlCondition, params object[] args)
        {
            try
            {
                return _dbContext.DbInstance.Exists<T>(sqlCondition, args);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }

        public bool Exists(string field, object value)
        {
            try
            {
                return _dbContext.DbInstance.Exists<T>(string.Format("Where {0} = @0", field), value);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }
        public bool Exists(string field, object value, out T entity)
        {
            try
            {
                // var sql = Sql.Builder.From(TableName).Where("@0 = @1", field, value);
                entity = _dbContext.DbInstance.FirstOrDefault<T>(string.Format("Where {0} = @0", field), value);
                if (DACS.IsNotEmpty(entity))
                    return true;
                entity = new T();
                return false;
            }
            catch (Exception ex)
            {
                entity = null;
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }
        public bool Exists(Sql sql, out T entity)
        {
            try
            {
                entity = _dbContext.DbInstance.FirstOrDefault<T>(sql);
                if (DACS.IsNotEmpty(entity))
                    return true;
                entity = new T();
                return false;
            }
            catch (Exception ex)
            {
                entity = null;
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }
        public bool ExistsNameUpdate(string name, object id)
        {
            try
            {
                return _dbContext.DbInstance.Exists<T>("@Name = @0 and ID <> @id", name, id);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }

        public List<T> GetItems(params object[] ids)
        {
            Sql sql = Sql.Builder.From(TableName).Where("ID in (@0)", ids);
            return GetItems(sql);
        }

        public List<T> GetListByField(string field, object value)
        {
            try
            {
                return _dbContext.DbInstance.Fetch<T>(string.Format("Where {0} = @0", field), value);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }
        public T GetListField(string field, object value)
        {
            try
            {
                return _dbContext.DbInstance.Single<T>(string.Format("Where {0} = @0", field), value);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }
        public IEnumerable<T> GetWithoutItems(params object[] ids)
        {
            try
            {
                Sql sql = Sql.Builder.From(TableName).Where("ID NOT IN (@0)", ids);
                return GetItems(sql);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }


        }

        public IEnumerable<T> GetListByFieldWithoutItems(string field, params object[] fields)
        {
            try
            {
                return _dbContext.DbInstance.Fetch<T>(string.Format("Where {0} NOT INT (@0)", field), fields);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }
        public List<T> Search(CommonParam param, Pagination paging)
        {
            Sql sql = Sql.Builder.From(TableName);
            var query = param.GetQuery();
            if (!String.IsNullOrEmpty(query))
                sql.Where(query);
            var result = Page(sql, paging.PageIndex, paging.PageSize);
            paging.RowCount = (int)result.TotalItems;
            return result.Items;
        }
        public List<T> Search(CommonParam param)
        {
            Sql sql = Sql.Builder.From(TableName);
            var query = param.GetQuery();
            if (!String.IsNullOrEmpty(query))
                sql.Where(query);
            return GetItems(sql);
        }

        public bool Updates(List<T> pocos, params string[] columns)
        {
            try
            {
                bool result = true;
                _unitOfWork.BeginTransaction();
                if (DACS.IsEmpty<T>(pocos))
                    return false;
                foreach (var item in pocos)
                {
                   result =result && Update(item, columns);
                }
                if (result)
                    _unitOfWork.CommitTransaction();
                return result;
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex.ToString(), TableName);
                return false;
            }
            finally
            {
                _unitOfWork.AbortTransaction();
            }
        }

        public bool Delete(object id)
        {
            try
            {
                return _dbContext.DbInstance.Delete(id) >=0;
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return false;
            }
        }

        public List<T> GetListByFields(string field, params object[] values)
        {
            Sql sql = Sql.Builder.From(TableName).Where(string.Format("{0} in (@0)", field), values);
            return GetItems(sql).ToList();
        }
        public T GetFirstByField(string field, object value)
        {
            try
            {
                return _dbContext.DbInstance.First<T>(string.Format("Where {0} = @0", field), value);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }
        /// <summary>
        /// Lấy bản ghi đầu tiên theo trường và danh sách giá trị
        /// </summary>
        /// <param name="field">Tên trường</param>
        /// <param name="values">Danh sách các trường</param>
        /// <returns></returns>
        public T GetFirstByFields(string field, params object[] values)
        {
            try
            {
                return _dbContext.DbInstance.First<T>(string.Format("Where {0} in( @0)", field), values);
            }
            catch (Exception ex)
            {
                DacLoger.Log(ex, _tableName);
                return null;
            }
        }

        
    }
}
