﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace ShopAdmin.Controllers
{
    public class SigninController : Controller
    {
        // GET: Signin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignIn()
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.PrimarySid, "dac"));
            claims.Add(new Claim(ClaimTypes.Role, "Add"));
            claims.Add(new Claim(ClaimTypes.Role, "Edit"));
            claims.Add(new Claim(ClaimTypes.Role, "Delete"));
            var claimsIdentity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = true,
            }, claimsIdentity);
            var c = (HttpContext.User.Identity as ClaimsIdentity);
            c.FindFirst(ClaimTypes.Name);
            return View();
        }
    }
}