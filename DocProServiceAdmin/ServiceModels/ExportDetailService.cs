﻿using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopRetailCore.Interfaces;
using ShopRetailServiceAdmin.Context;
using ShopRetailCore.Context;

namespace ShopRetailServiceAdmin.ServiceModels
{
    public class ExportDetailService: Repository<ExportDetail>, IExportDetail
    {
        public ExportDetailService(IDbContextAdmin dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {

        }
    }
}
