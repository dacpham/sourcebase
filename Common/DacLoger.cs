﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Common.Dac
{
    public class DacLoger
    {
        // Fields
        private static readonly object LockWriter = new object();

        // Methods
        private static string GetFullName(string nameNoExt)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("App_Data/Log/{0}.txt", nameNoExt));
            string directoryName = Path.GetDirectoryName(path);
            if (directoryName == null)
            {
                return null;
            }
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
            return path;
        }

        public static void Log(object e, string fileName = "log")
        {
            if (e != null)
            {
                Log(e.ToString(), fileName);
            }
        }

        public static void Log(string e, string fileName = "log")
        {
            if (!string.IsNullOrEmpty(e))
            {
                lock (LockWriter)
                {
                    try
                    {
                        string fullName = GetFullName(fileName);
                        string str2 = DateTime.Now.ToString("dd-MM-yyyy H:m:s");
                        using (StreamWriter writer = new StreamWriter(fullName, true))
                        {
                            writer.WriteLine("\n\n[{0}]:\t{1}", str2, e);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        public static void Logs(IList data, string fileName = "log")
        {
            if (!object.Equals(data, null))
            {
                Log(string.Join(", ", data), fileName);
            }
        }

        public static void Logs(List<string> data, string fileName = "log")
        {
            if (!object.Equals(data, null) && data.Any<string>())
            {
                Log(string.Join("\n", data), fileName);
            }
        }

        public static StreamWriter StreamWriter(string fileName = "log")
        {
            return new StreamWriter(GetFullName(fileName), true);
        }
    }




  public class Loger
    {
        // Fields
        private static readonly object LockWriter = new object();

        // Methods
        private static string GetFullName(string nameNoExt)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("App_Data/Log/{0}.txt", nameNoExt));
            string directoryName = Path.GetDirectoryName(path);
            if (directoryName == null)
            {
                return null;
            }
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
            return path;
        }

        public static void Log(object e, string fileName = "log")
        {
            if (e != null)
            {
                Log(e.ToString(), fileName);
            }
        }

        public static void Log(string e, string fileName = "log")
        {
            if (!string.IsNullOrEmpty(e))
            {
                lock (LockWriter)
                {
                    try
                    {
                        string fullName = GetFullName(fileName);
                        string str2 = DateTime.Now.ToString("dd-MM-yyyy H:m:s");
                        using (StreamWriter writer = new StreamWriter(fullName, true))
                        {
                            writer.WriteLine("\n\n[{0}]:\t{1}", str2, e);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        public static void Logs(List<int> data, string fileName = "log")
        {
            if (!object.Equals(data, null) && data.Any<int>())
            {
                Log(string.Join<int>(", ", data), fileName);
            }
        }

        public static void Logs(List<string> data, string fileName = "log")
        {
            if (!object.Equals(data, null) && data.Any<string>())
            {
                Log(string.Join("\n", data), fileName);
            }
        }

        public static StreamWriter StreamWriter(string fileName = "log")
        {
            return new StreamWriter(GetFullName(fileName), true);
        }
    }



}
