﻿using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Models
{
    public class ProductModel :Product
    {
        public List<Event> Events { get; set; }
        public List<StgFiles> Files { get; set; }
        public Category Category { get; set; }
       
    }
}