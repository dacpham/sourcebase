﻿using ShopRetail.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Timers;
using System.Web;
using System.Web.Configuration;

namespace ShopRetail.Customs.Helper
{
    public class SendMailHelper
    {
        public SendMailHelper()
        {
        }
    
        #region ------ Send Mail -------
        public static bool SendMail(MailModel mail)
        {
            try
            {
                var configurationFile = WebConfigurationManager.OpenWebConfiguration("~/web.config");
                var mailSettings = configurationFile.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                if (Equals(mailSettings, null))
                {
                    return false;
                }
                int port = mailSettings.Smtp.Network.Port;
                string host = mailSettings.Smtp.Network.Host;
                string password = mailSettings.Smtp.Network.Password;
                string username = mailSettings.Smtp.Network.UserName;
                using (MailMessage mm = new MailMessage())
                {
                    mm.Subject = mail.subject;
                    mm.Body = mail.body;
                    mm.IsBodyHtml = true;
                    mm.From = new MailAddress(username, mail.displayname);
                    mm.To.Add(mail.mailto);
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = host;
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(username, password);
                    smtp.Port = port;
                    smtp.Send(mm);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

    }
}