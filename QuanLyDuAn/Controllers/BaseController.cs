﻿using Common.Dac;
using Common.Extensions;
using QuanLyDuAn.Customs.Entities;
using QuanLyDuAn.Customs.Params;
using QuanLyDuAn.Customs.Utility;
using QuanLyDuAn.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ShopAdmin.Controllers
{
    public class BaseController : Controller
    {
        #region-------Khởi tạo-----------------
        protected IUser _resUser;
        protected ICategory _resCategory;
        protected IEvent _resEvent;
        protected IExport _resExport;
        protected IExportDetail _resExportDetail;
        protected IGuest _resGuest;
        protected Import _resImport;
        protected IOrder _resOrder;
        protected IOrderDetail _resOrderDetail;
        protected IProduct _resProduct;
        protected IProductViewed _resProductViewed;
        protected ISliderInfor _resSliderInfor;
        protected IStatus _resStatus;
        protected IStgFiles _resStgFiles;
        protected IAccount _resAccount;
        protected IDept _resDept;
        protected IPosition _resPosition;
        protected IModule _resModule;
        protected IRole _resRole;
        protected IRoleModule _resRoleModule;
        protected IAccountRole _resAccountRole;
        #endregion-----------------------------

        public BaseController(
            IUser resUser, 
            ICategory resCategory,
            IExport resExport,
            IEvent resEvent,
            IExportDetail resExportDetail,
            IGuest resGuest,
            Import resImport,
            IOrder resOrder,
            IOrderDetail resOrderDetail,
            IProduct resProduct,
            IProductViewed resProductViewed,
            ISliderInfor resSliderInfor,
            IStatus resStatus,
            IStgFiles resStgFiles,
            IAccount resAccount,
            IDept resDept,
            IPosition resPosition,
            IModule resModule,
            IRole resRole,
            IRoleModule resRoleModule,
            IAccountRole resAccountRole)
        {
            _resUser = resUser;
            _resCategory = resCategory;
            _resExport = resExport;
            _resEvent = resEvent;
            _resExportDetail = resExportDetail;
            _resGuest = resGuest;
            _resImport = resImport;
            _resOrder = resOrder;
            _resOrderDetail = resOrderDetail;
            _resProduct = resProduct;
            _resProductViewed = resProductViewed;
            _resSliderInfor = resSliderInfor;
            _resStatus = resStatus;
            _resStgFiles = resStgFiles;
            _resAccount = resAccount;
            _resDept = resDept;
            _resPosition = resPosition;
            _resModule = resModule;
            _resRole = resRole;
            _resRoleModule = resRoleModule;
            _resAccountRole = resAccountRole;
        }

        /// <summary>
        ///     Title page
        /// </summary>
        public string area = "";// /admin
        private string _title;
        private readonly List<string> _warns = new List<string>();
        private readonly List<string> _errors = new List<string>();
        private readonly List<string> _success = new List<string>();
        private readonly List<string> _notifies = new List<string>();

        #region Form Data
        private Hashtable _data;
        public Hashtable DATA
        {
            get
            {
                if (Equals(_data, null))
                    _data = DACS.GetDataPost();

                return _data;
            }
        }

        #endregion

        #region ResResult

        private bool _isLogout;
        private bool _isMsg;

        private bool _isDL;
        private object _wDL;
        private string _htDL;

        private bool _isCust;
        private string _htCust;

        private bool _resOnlyData;
        private dynamic _dataRes;

        internal void SetIsLogout()
        {
            _isLogout = true;
        }
        internal void SetDataResponse(dynamic data)
        {
            _dataRes = data;
        }
        internal void SetOnlyDataResponse(dynamic data)
        {
            _resOnlyData = true;
            _dataRes = data;
        }
        /// <summary>
        /// Set html of dialog
        /// </summary>
        /// <param name="html"></param>
        internal void SetHtmlDialog(string html, object width)
        {
            _isDL = true;
            _wDL = width;
            _htDL = html;
        }

        /// <summary>
        /// Set html of custom placed
        /// </summary>
        /// <param name="html"></param>
        internal void SetHtmlResponse(string html)
        {
            _isCust = true;
            _htCust = html;
        }

        public JsonResult GetResult()
        {
            if (_resOnlyData)
                return Json(new Hashtable {
                    {"data", _dataRes ?? string.Empty}
                },JsonRequestBehavior.AllowGet);

            var res = new Hashtable();
            res.Add("data", _dataRes ?? string.Empty);
            res.Add("title", ViewBag.Title ?? string.Empty);

            if (_errors.Any())
                res.Add("isErr", 1);
            if (_isLogout)
                res.Add("isLogout", 1);

            if (_isMsg)
            {
                var messages = new List<string>();
                messages.AddRange(_errors);
                messages.AddRange(_warns);
                messages.AddRange(_notifies);
                messages.AddRange(_success);

                res.Add("isMsg", 1);
                res.Add("isError", HasError);
                res.Add("isWarn", HasWarn);
                res.Add("isNotify", HasNotify);
                res.Add("isSuccess", HasSuccess);

                res.Add("msError", string.Join("\n", _errors));
                res.Add("msWarn", string.Join("\n", _warns));
                res.Add("msNotify", string.Join("\n", _notifies));
                res.Add("msSuccess", string.Join("\n", _success));
                res.Add("htMsg", GetMessages());
            }
            if (_isDL)
            {
                res.Add("isDL", 1);
                res.Add("wDL", _wDL);
                res.Add("htDL", _htDL ?? string.Empty);
            }
            if (_isCust)
            {
                res.Add("isCust", 1);
                res.Add("htCust", _htCust ?? string.Empty);
            }
            return Json(res, JsonRequestBehavior.AllowGet);
            //json.MaxJsonLength = int.MaxValue;
        }

        #endregion

        #region RenderOptions
        //public static string RenderOptions(dynamic data, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, hasUndefined));
        //}
        //public static string RenderOptions(dynamic data, int selected, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, selected, hasUndefined));
        //}
        //public static string RenderOptions(dynamic data, List<int> selecteds, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, selecteds, hasUndefined));
        //}
        #endregion

        /// <summary>
        ///     Session ID
        /// </summary>
        public string SessionID
        {
            get
            {
                try
                {
                    return Session.SessionID;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        // <summary>
        //     Current user
        // </summary>
        public Guest CGuest;

        // <summary>
        //     Current user
        // </summary>
        public Account CUser;

        /// <summary>
        ///     Pagination
        /// </summary>
        public Pagination Paging;

        /// <summary>
        ///     Stylesheets
        /// </summary>
        public List<string> Css;

        /// <summary>
        ///     Javascripts
        /// </summary>
        public List<string> Scripts;
        internal bool HasError
        {
            get { return _errors.Any(); }
        }
        internal bool HasWarn
        {
            get { return _warns.Any(); }
        }
        internal bool HasNotify
        {
            get { return _notifies.Any(); }
        }
        internal bool HasSuccess
        {
            get { return _success.Any(); }
        }

        /// <summary>
        ///     Set title page
        /// </summary>
        /// <param name="title"></param>
        public void SetTitle(string title)
        {
            _title = title;
        }

        /// <summary>
        ///     Set success
        /// </summary>
        /// <param name="success"></param>
        internal void SetSuccess(string success)
        {
            _isMsg = true;
            _success.Add(success);
            Session["Success"] = _success;
        }
        internal void SetSuccess(List<string> success)
        {
            if (!Equals(success, null) && success.Any())
            {
                _isMsg = true;
                _success.AddRange(success);
                Session["Success"] = _success;
            }
        }
        internal void SetNotify(string notify)
        {
            _isMsg = true;
            _notifies.Add(notify);
            Session["Notifies"] = _notifies;
        }
        internal void SetNotify(List<string> notifies)
        {
            if (!Equals(notifies, null) && notifies.Any())
            {
                _isMsg = true;
                _notifies.AddRange(notifies);
                Session["Notifies"] = _notifies;
            }
        }

        /// <summary>
        ///     Set errors
        /// </summary>
        /// <param name="error"></param>
        internal void SetError(string error)
        {
            _isMsg = true;
            _errors.Add(error);
            Session["Errors"] = _errors;
        }
        internal void SetErrors(List<string> errors)
        {
            if (!Equals(errors, null) && errors.Any())
            {
                _isMsg = true;
                _errors.AddRange(errors);
                Session["Errors"] = _errors;
            }
        }

        /// <summary>
        ///     Set warnings
        /// </summary>
        /// <param name="warn"></param>
        internal void SetWarn(string warn)
        {
            _isMsg = true;
            _warns.Add(warn);
            Session["Warns"] = _warns;
        }
        internal void SetWarns(List<string> warns)
        {
            if (!Equals(warns, null) && warns.Any())
            {
                _isMsg = true;
                _warns.AddRange(warns);
                Session["Warns"] = _warns;
            }
        }

        /// <summary>
        ///     Include stylesheet
        /// </summary>
        /// <param name="item"></param>
        public void IncludeCss(string item)
        {
            if (Equals(Css, null))
                Css = new List<string>();

            Css.Add(item);
        }

        /// <summary>
        ///     Include javascript
        /// </summary>
        /// <param name="item"></param>
        public void IncludeScript(string item)
        {
            if (Equals(Scripts, null))
                Scripts = new List<string>();

            Scripts.Add(item);
        }

        /// <summary>
        ///     Called before the action method is invoked.
        /// </summary>
        /// <param name="filterContext">
        ///     Information about the current request and action.
        /// </param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            ViewBag.Title = ("LShoper");
            Paging = new Pagination(HttpContext.Request);
            GetCUser();
        }
        public void GetCUser()
        {
            CUser = new Account();// AclConfig.CurrentGuest; DacPV
            ViewBag.CUser = CUser;
        }
        /// <summary>
        ///     Called after the action method is invoked.
        /// </summary>
        /// <param name="filterContext">
        ///     Information about the current request and action.
        /// </param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (Request.IsAjaxRequest() == false)
            {
                //if (CUser.ID > 0)
                //{
                //    //TODO DacPV
                //}

                #region Css, JS, CDATA

                //Is mobile
                if (Request.Browser.IsMobileDevice)
                {
                    IncludeCss("~/assets/jquery/css/jquery.mobile.css");
                    IncludeScript("~/assets/jquery/js/jquery.mobile.js");
                    IncludeScript("~/assets/jquery/js/jquery.plugins.js");
                    IncludeScript("~/assets/jquery/js/jquery.functions.js");
                }

                //Set title
                if (!Equals(_title, null))
                    ViewBag.Title = _title;

                //Set stylesheets
                if (!Equals(Css, null))
                    ViewBag.Css = Css.ToArray();

                //Set javascripts
                if (!Equals(Scripts, null))
                    ViewBag.Scripts = Scripts.ToArray();

                //Set pagination
                ViewBag.Pagination = Paging;
                //set dashboard

                //Set CDATA
                #endregion
            }
        }
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);

            if (!Equals(Session["user"], null))
                Session.Remove("user");
        }
        public ViewResult ViewDelete(DeleteParam deleteParam)
        {
            ViewBag.DeleteParam = deleteParam;
            if (!string.IsNullOrEmpty(deleteParam.Title))
                SetTitle(deleteParam.Title);
            return View("~/Views/Shared/IsDelete.cshtml");
        }
        public ViewResult ViewDeletes(DeletesParam deletesParam)
        {
            ViewBag.DeletesParam = deletesParam;
            if (!string.IsNullOrEmpty(deletesParam.Title))
                SetTitle(deletesParam.Title);
            return View("~/Views/Shared/IsDeletes.cshtml");
        }
        public string GetFormSearch(string name)
        {
            return string.IsNullOrEmpty(name)
                ? string.Empty
                : GetView(string.Format(
                    "~/Views/Shared/Searchs/{0}.cshtml",
                    name
                ));
        }
        public string GetView(string viewName, object data = null)
        {
            try
            {
                ViewBag.Pagination = Paging;
                if (!Equals(_title, null))
                    ViewBag.Title = _title;

                using (var sw = new StringWriter())
                {
                    ControllerContext.Controller.ViewData.Model = data;
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.ToString();
                }
            }
            catch (Exception ex)
            {
                //Loger.Log(ex);
                return string.Empty;
            }
        }
        public string GetMessages()
        {
            return GetView("~/Views/Shared/Message.cshtml");
        }
        public string GetReferrerOrDefault(string defaultPath)
        {
            if (Equals(Request.UrlReferrer, null))
                return defaultPath;

            return Request.UrlReferrer.ToString();
        }
        public string GetRedirectOrDefault(string defaultPath)
        {
            var redirectPath = DACS.GetString(DATA, "RedirectPath");
            return string.IsNullOrEmpty(redirectPath)
                ? defaultPath
                : redirectPath;
        }
        public ActionResult GetResultOrRedirectDefault(string defaultPath)
        {
            if (Request.IsAjaxRequest())
                return GetResult();
            return RedirectToPath(GetRedirectOrDefault(defaultPath));
        }
        public ActionResult GetResultOrReferrerDefault(string defaultPath)
        {
            if (Request.IsAjaxRequest())
                return GetResult();
            return RedirectToPath(GetReferrerOrDefault(defaultPath));
        }
        public ActionResult GetResultOrView(string viewName, object data = null)
        {
            if (Request.IsAjaxRequest())
            {
                return GetResult();
            }
            return GetViewResult(viewName, data);
        }
        public ActionResult GetCustResultOrView(ViewParam viewParam)
        {
            if (Request.IsAjaxRequest())
            {
                if (!Equals(viewParam.Data, null))
                    SetDataResponse(viewParam.Data);

                SetHtmlResponse(GetView(viewParam.ViewNameAjax ?? viewParam.ViewName, viewParam.Data));
                return GetResult();
            }
            return GetViewResult(viewParam.ViewName, viewParam.Data);
        }
        public ActionResult GetDialogResultOrView(ViewParam viewParam)
        {
            if (Request.IsAjaxRequest())
            {
                if (!Equals(viewParam.Data, null))
                    SetDataResponse(viewParam.Data);

                SetHtmlDialog(GetView(viewParam.ViewNameAjax ?? viewParam.ViewName,
                    viewParam.Data
                ), viewParam.Width ?? 600);
                return GetResult();
            }
            return GetViewResult(viewParam.ViewName, viewParam.Data);
        }
        public ActionResult GetCustResultOrView(string viewName, object data = null)
        {
            if (Request.IsAjaxRequest())
            {
                //if (!Equals(data, null))
                //    SetDataResponse(data);

                SetHtmlResponse(GetView(viewName, data));
                return GetResult();
            }
            return GetViewResult(viewName, data);
        }
        public ActionResult GetDialogResultOrView(string viewName, object data = null, object w = null)
        {
            if (Request.IsAjaxRequest())
            {
                //if (!Equals(data, null))
                //    SetDataResponse(data);

                SetHtmlDialog(GetView(viewName, data), w ?? 600);
                return GetResult();
            }
            return GetViewResult(viewName, data);
        }
        public ActionResult GetViewResult(string viewName, object data = null)
        {
            return View(viewName, data);

        }
        public RedirectResult RedirectToPath(string path)
        {
            path = (path ?? "/");
            return Redirect(path);
        }
        public RedirectResult RedirectToPath(string path, params object[] param)
        {
            return Redirect(string.Format(path, param));
        }
        protected ActionResult GetDialogResultOrViewConfirm(ConfirmParam confirmParam, object w = null)
        {
            ViewBag.ConfirmParam = confirmParam;
            if (!string.IsNullOrEmpty(confirmParam.Title))
                SetTitle(confirmParam.Title);

            return GetDialogResultOrView("~/Views/Shared/IsConfirm.cshtml", Equals(w, null) ? 500 : w);
        }
        protected ActionResult GetDialogResultOrViewConfirms(ConfirmsParam confirmsParam, object w = null)
        {
            ViewBag.ConfirmsParam = confirmsParam;
            if (!string.IsNullOrEmpty(confirmsParam.Title))
                SetTitle(confirmsParam.Title);

            return GetDialogResultOrView("~/Views/Shared/IsConfirms.cshtml", Equals(w, null) ? 500 : w);
        }
        protected ActionResult GetDialogResultOrViewDelete(DeleteParam deleteParam, object w = null)
        {
            ViewBag.DeleteParam = deleteParam;
            if (!string.IsNullOrEmpty(deleteParam.Title))
                SetTitle(deleteParam.Title);

            return GetDialogResultOrView("~/Views/Shared/IsDelete.cshtml", Equals(w, null) ? 500 : w);
        }
        protected ActionResult GetDialogResultOrViewDeletes(DeletesParam deletesParam, object w = null)
        {
            ViewBag.DeletesParam = deletesParam;
            if (!string.IsNullOrEmpty(deletesParam.Title))
                SetTitle(deletesParam.Title);

            return GetDialogResultOrView("~/Views/Shared/IsDeletes.cshtml", Equals(w, null) ? 500 : w);
        }
        

        /// <summary>
        /// lay  ra ten file  tu path (co random path)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected string GetNameFromPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return "";
            path = Path.GetFileName(path);
            var indexof = path.IndexOf("_");
            if (indexof < 0 || indexof == path.Length)
                return path;
            return path.Substring(indexof + 1);
        }
    }
}