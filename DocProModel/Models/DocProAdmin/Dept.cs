using PetaPoco;
namespace ShopRetailModel.Models
{
    [TableName("Dept")]
    [PrimaryKey("ID", AutoIncrement = true)]
    public partial class Dept : CreateUpdate
    {
        public int ID { get; set; } // int    

        [Column]
        public int IDChannel { get; set; } // int 

        [Column]
        public string Name { get; set; }

        [Column]
        public string Describe { get; set; }
        [Column]
        public string Code { get; set; }
        [Column]
        public int Parent { get; set; }

        [Column]
        public string Parents { get; set; }
        [Column]
        public string UserCount { get; set; }

        [Column]
        public string SearchMeta { get; set; }

    }
}
