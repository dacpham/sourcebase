﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Params
{
    public class EventParam
    {
        public string Term { get; set; }
        public string ID { get; set; }
    }
}
