﻿using System;
using System.IO;

namespace DocProResumable.Models
{
    public class ResumableConfiguration
    {
        /// <summary>
		/// Gets or sets number of expected chunks in this upload.
		/// </summary>
		public int Chunks { get; set; }
        public long TotalSize { get; set; }
        public string ContentType { get; set; }
        
        /// <summary>
        /// Gets or sets unique identifier for current upload.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets file name.
        /// </summary>
        public string FileName { get; set; }

        public ResumableConfiguration()
        {

        }

        /// <summary>
        /// Creates an object with file upload configuration.
        /// </summary>
        /// <param name="identifier">Upload unique identifier.</param>
        /// <param name="filename">File name.</param>
        /// <param name="chunks">Number of file chunks.</param>
        /// <returns>File upload configuration.</returns>
        public static ResumableConfiguration Create(string identifier, string filename, int chunks)
        {
            return new ResumableConfiguration { Identifier = identifier, FileName = filename, Chunks = chunks };
        }
    }
    public class CreateLogFiles
    {
        private string sLogFormat;
        private string sErrorTime;

        public CreateLogFiles()
        {
            //sLogFormat used to create log files format :
            // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
            sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            //this variable used to create log filename format "
            //for example filename : ErrorLogYYYYMMDD
            string sYear = DateTime.Now.Year.ToString();
            string sMonth = DateTime.Now.Month.ToString();
            string sDay = DateTime.Now.Day.ToString();
            sErrorTime = sYear + sMonth + sDay;
        }
        public void ErrorLog(string sPathName, string sErrMsg)
        {
            StreamWriter sw = new StreamWriter(sPathName + sErrorTime, true);
            sw.WriteLine(sLogFormat + sErrMsg);
            sw.Flush();
            sw.Close();
        }
    }
}