﻿using ShopRetailCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailServiceAdmin.Context
{
    public class DbContextAdmin : DocProDbContextGlobal, IDbContextAdmin
    {
        public DbContextAdmin():base("ShopRetailConnectionString")
        {
            
        }
    }
}
