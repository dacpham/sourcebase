﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PetaPoco;
using TestMultiInsert.Models;
using System.Data.SqlClient;

namespace TestMultiInsert
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Sql sql = Sql.Builder.Select("Table")
                .Where("ID =&0",10)
                .Where("ID =@0 ",2);
           
            BulkCopy();
            
        }
        private void BulkCopy()
        {
            using (var context = new DataTestContext())
            {
                var lst = context.Fetch<ROLE>("");
               // var list = context.Fetch<TestInsert>("");
                var items = new List<ROLE>();
                for (int i = 0; i < 10; i++)
                {
                    var item = new ROLE();
                    item.Name = "Name" + i;
                    item.IDChannel = i;
                    item.Parent =  i;
                    item.Parents = item.SEARCHMETA = "se" + i;;
                    items.Add(item);
                }

            var datatable=    Common.ConvertToDatatable<ROLE>(items);

                context.OpenSharedConnection();
                try
                {
                    SqlBulkCopy objbulk = new SqlBulkCopy(context.ConnectionString);
                    //assigning Destination table name  
                    objbulk.DestinationTableName = "Event";
                    //Mapping Table column  \
                   objbulk.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ID","ID"));
                    objbulk.ColumnMappings.Add(new SqlBulkCopyColumnMapping("IDChannel", "IDChannel"));
                    objbulk.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Name", "Name"));
                    objbulk.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Parent", "Parent"));
                    objbulk.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Parents", "Parents"));
                    objbulk.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Searchmeta", "searchmeta"));
                    //inserting bulk Records into DataBase   
                    objbulk.WriteToServer(datatable);
                }
                finally
                {
                    context.CloseSharedConnection();
                }

            }
        }

    }
}
