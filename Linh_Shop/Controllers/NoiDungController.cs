﻿
using Common.Dac;
using ShopRetail.Models;
using ShopRetailModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopRetail.Controllers
{
    public class NoiDungController : BaseController
    {
        public NoiDungController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, IImport resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IImportDetail resImportDetail, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resImportDetail, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
        }

        public ActionResult Index(int id=0)
        {
            var content = _resProduct.GetById(id);
            if (DACS.IsEmpty(content))
            {
                SetError(DACS.T("Nội dung không tồn tại"));
                return RedirectToPath("/home.html");
            }
            CContent = content;
            CCategory = _resCategory.GetById(content.IDCategory);
            ViewBag.LienQuans = _resProduct.GetListByField("IDCategory", content.IDCategory);
            var ovContent = DACS.CopyTo<ProductViewModel>(content);
            ovContent.CategoryName = CCategory.Name;
            return GetCustResultOrView("index", ovContent);
        }
	}
}