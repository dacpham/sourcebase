using PetaPoco;
namespace ShopRetailModel.Models
{
    [TableName("Role")]

    [PrimaryKey("ID", AutoIncrement = true)]

    public partial class Role : CreateUpdate
    {
        [Column]

        public int ID { get; set; } // int    
        [Column]

        public int IDChannel { get; set; } // int 

        [Column]

        public string Name { get; set; }

        [Column]
        public int Parent { get; set; }

        [Column]

        public string Parents { get; set; }

        [Column]

        public string SearchMeta { get; set; }

    }
}
