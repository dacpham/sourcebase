using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poco = PetaPoco;
namespace ShopRetailModel.Models
{
    [Poco.TableName("Module")]
    [Poco.PrimaryKey("ID", AutoIncrement = true)]
    public partial class Module : CreateUpdate
    {
        [Key]
        [Poco.Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ID { get; set; } // int    

        [Poco.Column]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Poco.Column]
        [Display(Name = "Describe")]
        public string Describe { get; set; }

        [Poco.Column]
        [Display(Name = "ModuleName")]
        public string ModuleName { get; set; }

        [Poco.Column]
        [Display(Name = "ModuleCode")]
        public string ModuleCode { get; set; }
    }
}
