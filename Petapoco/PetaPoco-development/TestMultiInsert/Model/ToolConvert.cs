﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestMultiInsert.Model
{
  public static  class ToolConvert
    {
        public static T CopySqlToOracle<T>(this T obj, object data)
        {
            if (!object.Equals(data, null))
            {
                foreach (PropertyInfo info in obj.GetType().GetProperties())
                {
                    try
                    {
                        Type propertyType = info.PropertyType;
                        var value = GetValueOfObject(data, info.Name);
                        info.SetValue(obj, value);
                    }
                    catch
                    {
                    }
                }
            }
            return obj;
        }
        public static object GetValueOfObject(object data, string name)
        {
            foreach (var info in data.GetType().GetProperties())
            {
                if (info.Name.ToUpper() == name.ToUpper())
                {
                    if (info.PropertyType== typeof(bool) || info.PropertyType == typeof(bool?))
                    {
                        if (info.GetValue(data) == null)
                        {
                            return null;
                        }
                        else if ((bool)info.GetValue(data) == true)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return info.GetValue(data);
                    }
                }
            }
            return null;
        }
    }
}
