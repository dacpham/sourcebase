﻿using ShopRetailCore.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Params
{
    public class RoleModuleParam : CommonParam
    {
        public string Term { get; set; }
        public long ID { get; set; }
        public long Parent { get; set; }
        public override string GetQuery()
        {
            string result = string.Empty;
            var conditions = new List<string>();
            if (!string.IsNullOrEmpty(Term))
            {
                conditions.Add(string.Format("Name like N'%{0}%'", Term));
            }
            if (ID > 0)
            {
                conditions.Add(string.Format("ID = {0}", ID));
            }
            if (Parent >= 0)
            {
                conditions.Add(string.Format("Parent = {0}", Parent));
            }
            if (conditions.Count > 0)
                result = string.Join(" AND ", conditions);
            return result;
        }
    }
}
