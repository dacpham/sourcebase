﻿using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopAdmin.Controllers
{
    public class GiaoDienController : BaseController
    {
        // GET: GiaoDien

        public GiaoDienController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles, IAccount resAccount, IDept resDept, IPosition resPosition, IModule resModule, IRole resRole, IRoleModule resRoleModule, IAccountRole resAccountRole) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles, resAccount, resDept, resPosition, resModule, resRole, resRoleModule, resAccountRole)
        {
        }
        public ActionResult List()
        {
            return GetCustResultOrView("List", null);
        }
    }
}