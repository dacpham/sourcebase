﻿using DocProResumable.Models;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Hosting;
using DocProUtil;

namespace DocProResumable.Controllers
{
    public class ModelFile
    {
        public string Message { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public long Uploaded { get; set; }
        public long ContentLength { get; set; }
        public bool Flag { get; set; }

    }
    public class UpfileController : ApiController
    {
        private string dirChunkFile = HostingEnvironment.MapPath("~/upload");

        [HttpOptions]
        [ActionName("Upload")]
        public object UploadFileOptions()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [ActionName("Upload")]
        public object Upload(int resumableChunkNumber, string resumableIdentifier, long resumableTotalSize)
        {
            if (resumableTotalSize > CheckFreeSpaceDisk())
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return ChunkIsHere(resumableChunkNumber, resumableIdentifier) 
                ? Request.CreateResponse(HttpStatusCode.OK) 
                : Request.CreateResponse(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [ActionName("Upload")]
        public async Task<object> Upload()
        {
            Utils.CreateDir(dirChunkFile);

            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartFormDataStreamProvider(dirChunkFile);
            var result = await readPart(provider);
            if (result.Flag)
            {
                return result;
            }
            else
            {
                DeleteInvalidChunkData(provider);

                return result;
            }
        }


        private static bool DeleteInvalidChunkData(MultipartFormDataStreamProvider provider)
        {
            try
            {
                var localFileName = provider.FileData[0].LocalFileName;
                Utils.DeleteFile(localFileName);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<ModelFile> readPart(MultipartFormDataStreamProvider provider)
        {
            int chunkNumber = 0;
            try
            {

                await Request.Content.ReadAsMultipartAsync(provider);
                var configuration = GetUploadConfiguration(provider);

                // Get chunk number
                chunkNumber = GetChunkNumber(provider);

                // Rename generated file
                // Only one file in multipart message
                MultipartFileData chunk = provider.FileData[0]; 

                RenameChunk(chunk, chunkNumber, configuration.Identifier);

                // Assemble chunks into single file if they're all here
                var result = TryAssembleFile(configuration);
                    result.Flag = true;

                return result;
            }
            catch (Exception ex)
            {
                Loger.Log(ex, "readPart");
                return new ModelFile { Flag = false };
            }
        }

        #region Get configuration

        [NonAction]
        private ResumableConfiguration GetUploadConfiguration(MultipartFormDataStreamProvider provider)
        {
            return ResumableConfiguration.Create(
                identifier: GetId(provider), 
                filename: GetFileName(provider), 
                chunks: GetTotalChunks(provider)
            );
        }

        [NonAction]
        private string GetFileName(MultipartFormDataStreamProvider provider)
        {
            var filename = provider.FormData["resumableFilename"];
            return !string.IsNullOrEmpty(filename) ? filename : provider.FileData[0].Headers.ContentDisposition.FileName.Trim('\"');
        }

        [NonAction]
        private string GetId(MultipartFormDataStreamProvider provider)
        {
            var id = provider.FormData["resumableIdentifier"];
            return !string.IsNullOrEmpty(id) ? id : Guid.NewGuid().ToString();
        }

        [NonAction]
        private int GetTotalChunks(MultipartFormDataStreamProvider provider)
        {
            var total = provider.FormData["resumableTotalChunks"];
            return !string.IsNullOrEmpty(total) ? Convert.ToInt32(total) : 1;
        }

        [NonAction]
        private int GetChunkNumber(MultipartFormDataStreamProvider provider)
        {
            var chunk = provider.FormData["resumableChunkNumber"];
            return !string.IsNullOrEmpty(chunk) ? Convert.ToInt32(chunk) : 1;
        }

        #endregion

        #region Chunk methods

        [NonAction]
        private string GetChunkFileName(int chunkNumber, string identifier)
        {
            return Path.Combine(dirChunkFile, string.Format("{0}_{1}", identifier, chunkNumber.ToString()));
        }

        [NonAction]
        private void RenameChunk(MultipartFileData chunk, int chunkNumber, string identifier)
        {
            try
            {

                string generatedFileName = chunk.LocalFileName;
                string chunkFileName = GetChunkFileName(chunkNumber, identifier);

                //delete file neu ton tai
                Utils.DeleteFile(chunkFileName);

                //move file moi
                Utils.MoveFile(generatedFileName, chunkFileName);

            }
            catch (Exception ex)
            {
                Loger.Log(ex, "RenameChunk");
            }
        }

        [NonAction]
        private string GetFilePath(ResumableConfiguration configuration)
        {
            return Path.Combine(dirChunkFile, configuration.Identifier);
        }

        [NonAction]
        private bool ChunkIsHere(int chunkNumber, string identifier)
        {
            string fileName = GetChunkFileName(chunkNumber, identifier);
            return Utils.IsFileExists(fileName);
        }

        [NonAction]
        private bool AllChunksAreHere(ResumableConfiguration configuration)
        {
            for (int chunkNumber = 1; chunkNumber <= configuration.Chunks; chunkNumber++)
                if (!ChunkIsHere(chunkNumber, configuration.Identifier)) return false;

            return true;
        }

        [NonAction]
        private ModelFile TryAssembleFile(ResumableConfiguration configuration)
        {
            try
            {
                ModelFile result = new ModelFile();
                if (AllChunksAreHere(configuration))
                {
                    // Create a single file
                    var sourcePath = ConsolidateFile(configuration);

                    // Get target file
                    var absPath = string.Empty;
                    var targetPath = Utils.GetSavePath(configuration.FileName, out absPath);

                    result.FileName = configuration.FileName;
                    result.FilePath = absPath;
                    result.Uploaded = DateTime.Now.Ticks;
                    result.ContentLength = configuration.TotalSize;
                    result.ContentType = configuration.ContentType;

                    //Rename consolidated with original name of upload
                    //RenameFile(path, Path.Combine(dirChunkFile, configuration.FileName));

                    RenameFile(sourcePath, targetPath);

                    // Delete chunk files
                    DeleteChunks(configuration);
                }
                return result;
            }
            catch (Exception ex)
            {
                Loger.Log(ex.Message, "TryAssembleFile");
                return new ModelFile
                {
                    Message = Locate.T("Lưu file không thành công")
                };
            }
        }

        [NonAction]
        private void DeleteChunks(ResumableConfiguration configuration)
        {
            for (int chunkNumber = 1; chunkNumber <= configuration.Chunks; chunkNumber++)
            {
                var chunkFileName = GetChunkFileName(chunkNumber, configuration.Identifier);
                Utils.DeleteFile(chunkFileName);
            }
        }

        [NonAction]
        private string ConsolidateFile(ResumableConfiguration configuration)
        {
            var path = GetFilePath(configuration);

            //ghep cac chunks vao thanh 1 file
            using (var destStream = File.Create(path, 15000))
            {
                for (int chunkNumber = 1; chunkNumber <= configuration.Chunks; chunkNumber++)
                {
                    var chunkFileName = GetChunkFileName(chunkNumber, configuration.Identifier);
                    using (var sourceStream = File.OpenRead(chunkFileName))
                    {
                        sourceStream.CopyTo(destStream);
                    }
                }
                destStream.Close();
            }

            var fileInfo = new FileInfo(path);
            configuration.TotalSize = fileInfo.Length;
            configuration.ContentType = Utils.GetContentType(Path.GetExtension(path));

            return path;
        }

        #endregion

        [NonAction]
        private string RenameFile(string sourcePath, string targetPath)
        {
            // Strip to filename if directory is specified (avoid cross-directory attack)
            var targetName = Path.GetFileName(targetPath);

            //di chuyen file vao target
            Utils.MoveFile(sourcePath, targetPath);

            return targetName;
        }

        [NonAction]
        private long CheckFreeSpaceDisk()
        {
            var dir = new DirectoryInfo(dirChunkFile);
            var dDrive = new DriveInfo(dir.Root.Name);

            // When the drive is accessible..
            if (dDrive.IsReady)
            {
                return dDrive.AvailableFreeSpace; //byte
            }
            return 1;
        }
    }
}
