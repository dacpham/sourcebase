﻿using QuanLyDuAn.Models;
using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using Common.Dac;
using QuanLyDuAn.Customs.Enum;
using ShopRetailModel.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Params;
using QuanLyDuAn.Customs.Params;

namespace ShopAdmin.Controllers
{
    public class PositionController : BaseController
    {
        private string defaultPath = "/position.html";
        public PositionController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles, IAccount resAccount, IDept resDept, IPosition resPosition, IModule resModule, IRole resRole, IRoleModule resRoleModule, IAccountRole resAccountRole) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles, resAccount, resDept, resPosition, resModule, resRole, resRoleModule, resAccountRole)
        {
        }

        #region List
        public ActionResult Index(int id = 0)
        {
            SetTitle(DACS.T("Danh sách chức vụ"));
            var param = DACS.Bind<PositionParam>(DATA);
            param.Parent = id;
            var positions = _resPosition.Search(param, Paging);
            var model = new PositionModel()
            {
                Positions = positions,
                SearchParam = param,
                Url = DACS.ReUrl("/position.html")
            };
            return GetCustResultOrView(new ViewParam() {
                ViewName = "Index",
                ViewNameAjax = "Positions",
                Data = model
            });
        }

     
        public ActionResult Create(int id = 0)
        {
            SetTitle(DACS.T("Tạo chức vụ mới"));
            var position = new Position();
            var parents = _resPosition.GetListByField("IDChannel", CUser.IDChannel);
            var model = new PositionModel()
            {
                Position = position,
                Parents = parents,
                SearchParam = new PositionParam() { Parent = id },
                Url = DACS.ReUrl("/position/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }
        public ActionResult Update(int id = 0)
        {
            SetTitle(DACS.T("Cập nhật chức vụ"));
            var position = _resPosition.GetById(id);
            if (DACS.IsEmpty(position))
            {
                SetError(DACS.T("Chức vụ không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var parents = _resPosition.GetListByField("IDChannel", CUser.IDChannel).Where(n => n.ID != id).ToList();
            var model = new PositionModel()
            {
                Position = position,
                Parents = parents,
                Url = DACS.ReUrl("/position/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }

        public ActionResult Save()
        {
            Position parent;
            Position entity;
            var position = DACS.Bind<Position>(DATA);
            if (position.ID == 0) // Insert
            {
                entity = DACS.BindCreate<Position>(position, CUser.ID);
                parent = _resPosition.GetById(entity.Parent) ?? new Position();
                entity.Parents = (DACS.IsNotEmpty(parent.Parents) ? parent.Parents + "|" : string.Empty) + entity.Parent;
                if (_resPosition.Insert(entity))
                {
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                entity = _resPosition.GetById(position.ID);
                entity = DACS.Bind<Position>(entity, DATA, 0, CUser.ID);
                parent = _resPosition.GetById(entity.Parent) ?? new Position();
                entity.Parents = (DACS.IsNotEmpty(parent.Parents) ? parent.Parents + "|" : string.Empty) + entity.Parent;
                if (_resPosition.Update(entity))
                {
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return RedirectToPath(DACS.ReUrl(DACS.admin + "/position.html?id={0}", entity.Parent.ToString()));

        }
        public ActionResult IsDelete(DeleteParam deleteParam)
        {
            var dept = _resPosition.GetById((int)deleteParam.ID);
            if (Equals(dept, null))
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDelete(new DeleteParam
            {
                ID = dept.ID,
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/dept/delete.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa chức vụ"),
                Title = DACS.T("Xác nhận xóa chức vụ")
            });
        }

        public ActionResult Delete(DeleteParam deleteParam)
        {
            var dept = _resPosition.GetById((int)deleteParam.ID);
            if (Equals(dept, null))
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            else
            {
                if (_resPosition.Delete(dept.ID))
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }

        public ActionResult IsDeletes(DeletesParam deletesParam)
        {
            if (!deletesParam.HasID)
            {
                SetWarn(DACS.T("Bạn chưa chọn chức vụ cần xóa"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var items = _resPosition.GetItems(deletesParam.IDToInts());
            if (!items.Any())
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDeletes(new DeletesParam
            {
                ID = items.Select(x => (long)x.ID).ToArray(),
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/dept/deletes.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa {0} chức vụ đã chọn?", items.Count),
                Title = DACS.T("Xác nhận xóa chức vụ")
            });
        }

        public ActionResult Deletes(DeletesParam deletesParam)
        {
            var depts = _resPosition.GetItems(deletesParam.IDToInts());
            if (!depts.Any())
                SetError(DACS.T("Phòng ban không còn tồn tại"));
            else
            {
                if (_resPosition.Deletes(deletesParam.ID) > 0)
                    SetSuccess(DACS.T("Xóa {0} chức vụ thành công", depts.Count));
                else
                    SetError(DACS.T("Lỗi: xóa chức vụ không thành công"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }
        #endregion
    }
}