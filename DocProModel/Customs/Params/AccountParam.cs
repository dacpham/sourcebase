﻿using ShopRetailCore.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Params
{
    public class AccountParam : CommonParam
    {
        public string Term { get; set; }
        public long ID { get; set; }
        public override string GetQuery()
        {
            string result = string.Empty;
            var conditions = new List<string>();
            if (!string.IsNullOrEmpty(Term))
            {
                conditions.Add(string.Format("Username like N'%{0}%'", Term));
            }
            if (ID > 0)
            {
                conditions.Add(string.Format("ID = {0}", ID));
            }
            if (conditions.Count > 0)
                result = string.Join(" AND ", conditions);
            return result;
        }
    }
}
