﻿using ShopRetailModel.Models;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Models
{
    public class AccountRoleModel
    {
        public List<AccountRole> AccountRoles { get; set; }
        public List<AccountRole> Parents { get; set; }
        public AccountRole AccountRole { get; set; }
        public AccountRoleParam SearchParam { get; set; }
        public string Url { get; set; }
    }
}