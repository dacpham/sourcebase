﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Models.QLDA
{

    [TableName("ProjectTypeFolder")]






    public partial class ProjectTypeFolder 
    {



        [Column]
        public int? ID { get; set; }





        [Column]
        public int? IDProjectType { get; set; }





        [Column]
        public string Name { get; set; }





        [Column]
        public string GroupName { get; set; }





        [Column]
        public string Describe { get; set; }





        [Column]
        public int? Day { get; set; }





        [Column]
        public int Weight { get; set; }





        [Column]
        public int CreatedBy { get; set; }





        [Column]
        public DateTime? Updated { get; set; }





        [Column]
        public int? UpdatedBy { get; set; }



    }
}
