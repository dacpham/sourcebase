﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace QuanLyDuAn.Customs.Utility
{
    public class CUtility
    {
        /// <summary>
        /// Lấy ngày thứ tiếng việt trong tuần (Thứ 2,Thứ 3,Hôm nay, Thứ 4....)
        /// </summary>
        /// <param name="date">thời gian đầu vào</param>
        /// <returns></returns>
        public static string getDayofWeek(DateTime date)
        {
            string result = string.Empty;
            string dayofweek = string.Empty;
            DateTime dtnow = DateTime.Now;
            switch ((int)date.DayOfWeek)
            {
                case 1:
                    dayofweek =string.Format("Thứ hai");
                    break;
                case 2:
                    dayofweek =string.Format("Thứ ba");
                    break;
                case 3:
                    dayofweek =string.Format("Thứ tư");
                    break;
                case 4:
                    dayofweek =string.Format("Thứ năm");
                    break;
                case 5:
                    dayofweek =string.Format("Thứ sáu");
                    break;
                case 6:
                    dayofweek =string.Format("Thứ bảy");
                    break;
                case 0:
                    dayofweek = string.Format("Chủ nhật");
                    break;
            }
            if (date.Year == dtnow.Year && date.Month == dtnow.Month && date.Day == dtnow.Day)
            {
                result =string.Format("Hôm nay, {0}, ngày {1}", dayofweek, date.ToString("dd/MM/yyyy"));
            }
            else
            {
                result =string.Format("{0}, ngày {1}", dayofweek, date.ToString("dd/MM/yyyy"));

            }
            return result;
        }

        public static Dictionary<int, string> EnumToDic<T>()
        {
            var dic = new Dictionary<int, string>();
            var values = System.Enum.GetValues(typeof(T));
            foreach (var item in values)
            {
                string description;
                try
                {
                    description = ((DescriptionAttribute)item.GetType().GetMember(item.ToString()).FirstOrDefault()
                        .GetCustomAttribute(typeof(DescriptionAttribute))).Description;
                }
                catch (Exception ex)
                {
                    description = item.ToString();
                }
                dic.Add((int)item, description);
            }
            return dic;
        }
    }
}