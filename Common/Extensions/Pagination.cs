﻿using Common.Dac;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Extensions
{
    public class Pagination
    {
        public Pagination()
        {

        }
        public Pagination(string value)
        {

        }
        public Pagination(HttpRequestBase value)
        {
            var queries = value.QueryString;
            if (queries["page"] != null)
            {
                int page = 0;
                int.TryParse(queries["page"], out page);
                this.PageIndex = page;
            }
            if (queries["np"] != null)
            {
                int np = 0;
                int.TryParse(queries["np"], out np);
                this.PageIndex = np == 0 ? 10 : np;
            }
            this.Path = value.RawUrl;
        }

        public Hashtable DataPost { get; set; }
        public int From { get; }
        public dynamic ID { get; set; }
        public string IDPager { get; set; }
        public long NumberPageOfDocs { get; set; }
        public int NumView { get; set; }
        public string Page { get; set; }
        /// <summary>
        /// Trang hiện thời
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// Số bản ghi trên 1 trang
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// Tổng số trang
        /// </summary>
        public int PageTotal
        {
            get
            {
                return (RowCount + PageSize - 1) / PageSize;
            }
        }
        public string Path { get; set; }
        public int RowCount { get; set; }
        public int RowStart { get; }
        public string Target { get; set; }
        public int To { get; }
        public string UrlQuery { get; set; }

        public string GetAttrs()
        {
            return "";
        }
        public string GetHtml()
        {
            return "";
        }
        public string GetNP()
        {
            return "";
        }
        public HtmlString Render()
        {
            var builder = new StringBuilder();
            builder.Append("<div class='dataTables_paginate paging_bootstrap' id='simpledatatable_paginate'>");
            if (this.RowCount > PageSize)
            {
                builder.Append("<div class='btn-group dropup'>");
                builder.Append(string.Format("<a href='#' data-toggle='dropdown' class='btn btn-sm btn-default dropdown-toggle'>10 <i class='fa fa-angle-down'></i></a><ul class='dropdown-menu dropdown-blue dropdown-menu-right'><li><a href='{0}'>10</a></li><li><a href='{1}'>20</a></li><li><a href='{2}'>30</a></li></ul>", DACS.SetParamForUrl(this.Path, "np", "10"), DACS.SetParamForUrl(this.Path, "np", "20"), DACS.SetParamForUrl(this.Path, "np", "30")));
                builder.Append("</div>");
                builder.Append("<ul class='pagination'>");
                if (this.PageIndex > 1)
                {
                    builder.Append(string.Format("<li class='prev'>{1}</li>", DACS.T("Quay lại"), GetLinkByTarget(this.Target, "", DACS.SetParamForUrl(this.Path, "page", this.PageIndex - 1), DACS.T("Quay lại"))));
                }
                for (int i = 1; i <= PageTotal; i++)
                {
                    builder.Append(string.Format("<li class='{0}'>{1}</li>", this.PageIndex == i ? "active" : "", GetLinkByTarget(this.Target, "", DACS.SetParamForUrl(this.Path, "page", i), i.ToString())));
                }
                if (this.PageIndex < this.PageTotal)
                {
                    builder.Append(string.Format("<li class='next'>{0}</li>", GetLinkByTarget(this.Target, "", DACS.SetParamForUrl(this.Path, "page", this.PageIndex + 1), DACS.T("Tiếp"))));
                }
                builder.Append("</ul>");
            }
            
            builder.Append("</div>");
            return new HtmlString( builder.ToString());
        }
        private string GetLinkByTarget(string target,string clas,string link,string namelink)
        {
            if (string.IsNullOrEmpty(target))
                return string.Format("<a class='{0}' {1} href='{2}'>{3} </a>", clas, "", link, namelink);
            return string.Format("<a class='{0}' {1} href='{2}'>{3} </a>",clas + " "+"QuickView","data-target='"+target+"'",link,namelink);
        }
        public Pagination SetNumView(int numView)
        {
            return this;
        }
        public Pagination SetPage(string page)
        {
            return this;
        }
        public Pagination SetParams(Hashtable args)
        {
            return this;
        }
        public Pagination SetPath(string path)
        {
            this.Path = path;
            return this;
        }
        public Pagination SetTarget(string target)
        {
            this.Target = target;
            return this;
        }
    }
}
