﻿using QuanLyDuAn.Models;
using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Dac;
using ShopRetailModel.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Params;
using QuanLyDuAn.Customs.Params;

namespace ShopAdmin.Controllers
{
    public class RoleController : BaseController
    {
        private string defaultPath = "/role.html";
        public RoleController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles, IAccount resAccount, IDept resDept, IPosition resPosition, IModule resModule, IRole resRole, IRoleModule resRoleModule, IAccountRole resAccountRole) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles, resAccount, resDept, resPosition, resModule, resRole, resRoleModule, resAccountRole)
        {
        }

        #region List
        public ActionResult Index(int id = 0)
        {
            SetTitle(DACS.T("Danh sách quyền"));
            var param = DACS.Bind<RoleParam>(DATA);
            param.Parent = id;
            var roles = _resRole.Search(param, Paging);
            var model = new RoleModel()
            {
                Roles = roles,
                SearchParam = param,
                Url = DACS.ReUrl("/role.html")
            };
            return GetCustResultOrView(new ViewParam()
            {
                ViewName = "Index",
                ViewNameAjax = "Roles",
                Data = model
            });
        }
        public ActionResult Create(int id = 0)
        {
            SetTitle(DACS.T("Tạo quyền mới"));
            var role = new Role();
            var parents = _resRole.GetListByField("IDChannel", CUser.IDChannel);
            var model = new RoleModel()
            {
                Role = role,
                Parents = parents,
                SearchParam = new RoleParam() { Parent = id },
                Url = DACS.ReUrl("/role/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }
        public ActionResult Update(int id = 0)
        {
            SetTitle(DACS.T("Cập nhật quyền"));
            var role = _resRole.GetById(id);
            if (DACS.IsEmpty(role))
            {
                SetError(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var parents = _resRole.GetListByField("IDChannel", CUser.IDChannel).Where(n => n.ID != id).ToList();
            var model = new RoleModel()
            {
                Role = role,
                Parents = parents,
                Url = DACS.ReUrl("/role/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }

        public ActionResult Save()
        {
            Role parent;
            Role entity;
            var role = DACS.Bind<Role>(DATA);
            if (role.ID == 0) // Insert
            {
                entity = DACS.BindCreate<Role>(role, CUser.ID);
                parent = _resRole.GetById(entity.Parent) ?? new Role();
                entity.Parents = (DACS.IsNotEmpty(parent.Parents) ? parent.Parents + "|" : string.Empty) + entity.Parent;
                if (_resRole.Insert(entity))
                {
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                entity = _resRole.GetById(role.ID);
                entity = DACS.Bind<Role>(entity, DATA, 0, CUser.ID);
                parent = _resRole.GetById(entity.Parent) ?? new Role();
                entity.Parents = (DACS.IsNotEmpty(parent.Parents) ? parent.Parents + "|" : string.Empty) + entity.Parent;
                if (_resRole.Update(entity))
                {
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return RedirectToPath(DACS.ReUrl(DACS.admin + "/role.html?id={0}", entity.Parent.ToString()));
        }

        public ActionResult IsDelete(DeleteParam deleteParam)
        {
            var role = _resRole.GetById((int)deleteParam.ID);
            if (Equals(role, null))
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDelete(new DeleteParam
            {
                ID = role.ID,
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/role/delete.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa quyền"),
                Title = DACS.T("Xác nhận xóa quyền")
            });
        }

        public ActionResult Delete(DeleteParam deleteParam)
        {
            var role = _resRole.GetById((int)deleteParam.ID);
            if (Equals(role, null))
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            else
            {
                if (_resRole.Delete(role.ID))
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }

        public ActionResult IsDeletes(DeletesParam deletesParam)
        {
            if (!deletesParam.HasID)
            {
                SetWarn(DACS.T("Bạn chưa chọn quyền cần xóa"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var items = _resRole.GetItems(deletesParam.IDToInts());
            if (!items.Any())
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDeletes(new DeletesParam
            {
                ID = items.Select(x => (long)x.ID).ToArray(),
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/role/deletes.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa {0} quyền đã chọn?", items.Count),
                Title = DACS.T("Xác nhận xóa quyền")
            });
        }

        public ActionResult Deletes(DeletesParam deletesParam)
        {
            var roles = _resRole.GetItems(deletesParam.IDToInts());
            if (!roles.Any())
                SetError(DACS.T("Phòng ban không còn tồn tại"));
            else
            {
                if (_resRole.Deletes(deletesParam.ID) > 0)
                    SetSuccess(DACS.T("Xóa {0} quyền thành công", roles.Count));
                else
                    SetError(DACS.T("Lỗi: xóa quyền không thành công"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }
        #endregion
    }
}