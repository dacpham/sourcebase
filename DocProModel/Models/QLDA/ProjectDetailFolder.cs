﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Models.QLDA
{
    [TableName("ProjectDetailFolder")]



    [PrimaryKey("ID", AutoIncrement = false)]




    public partial class ProjectDetailFolder 
    {



        [Column]
        public int ID { get; set; }





        [Column]
        public int? IDProject { get; set; }





        [Column]
        public string Name { get; set; }





        [Column]
        public int? Parent { get; set; }





        [Column]
        public string Parents { get; set; }





        [Column]
        public DateTime StartDate { get; set; }





        [Column]
        public DateTime EndDate { get; set; }





        [Column]
        public bool IsChild { get; set; }





        [Column]
        public int Weight { get; set; }





        [Column]
        public int CreatedBy { get; set; }





        [Column]
        public DateTime Created { get; set; }





        [Column]
        public DateTime? Updated { get; set; }





        [Column]
        public int? UpdatedBy { get; set; }



    }
}
