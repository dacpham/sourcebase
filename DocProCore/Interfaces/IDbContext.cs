﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailCore.Interfaces
{
    public interface IDbContext : IDisposable
    {
        Database DbInstance { get; }
    }
}
