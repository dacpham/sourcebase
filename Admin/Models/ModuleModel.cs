﻿using ShopRetailModel.Models;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Models
{
    public class ModuleModel
    {
        public List<Module> Modules { get; set; }
        public Module Module { get; set; }
        public ModuleParam SearchParam { get; set; }
        public string Url { get; set; }
    }
}