using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace ShopRetailModel.Models
{

    [TableName("Category")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class Category : CreateUpdate
    {
        [Column]
        public long ID { get; set; }
        [Column]
        public string Describe { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string Body { get; set; }
        [Column]
        public int Parent { get; set; }
        [Column]
        public string Parents { get; set; }
        [Column]
        public string Image { get; set; }
        [Column]
        public string Url { get; set; }
        [Column]
        public bool IsPublish { get; set; }
        [Column]
        public DateTime? Published { get; set; }
        [Column]
        public int? PublishBy { get; set; }
        [Column]
        public bool IsHeader { get; set; }
        [Column]
        public bool IsBody { get; set; }
    }



    [TableName("Event")]



    [PrimaryKey("ID", AutoIncrement = true)]




    [ExplicitColumns]

    public partial class Event : CreateUpdate
    {



        [Column]
        public int ID { get; set; }





        [Column]
        public string Name { get; set; }



    }



    [TableName("Export")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class Export : CreateUpdate
    {
        [Column]
        public int ID { get; set; }
        [Column]
        public int? IDDetail { get; set; }

        [Column]
        public int? Fee { get; set; }
        [Column]
        public long? Total { get; set; }
        [Column]
        public int? Promotion { get; set; }
        [Column]
        public string Note { get; set; }
        [Column]
        public int? IDGust { get; set; }
        [Column]
        public int? IDOrder { get; set; }

        [Column]
        public int? IDStatus { get; set; }
    }



    [TableName("ExportDetail")]
    [PrimaryKey("ID", AutoIncrement = true)]

    [ExplicitColumns]

    public partial class ExportDetail : CreateUpdate
    {

        [Column]
        public int ID { get; set; }
        [Column]
        public int IDProduct { get; set; }
        [Column]
        public int Price { get; set; }
        [Column]
        public int Number { get; set; }
        [Column]
        public long? Total { get; set; }
    }



    [TableName("Guest")]

    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class Guest : CreateUpdate
    {

        [Column]
        public int ID { get; set; }
        [Column]
        public string UserName { get; set; }
        [Column]
        public string PassWord { get; set; }
        [Column]
        public string FullName { get; set; }
        [Column]
        public string Avatar { get; set; }
        [Column]
        public DateTime? BirthDay { get; set; }
        [Column]
        public string Body { get; set; }
        [Column]
        public string Email { get; set; }

        [Column]
        public byte? TypeGuest { get; set; }
        [Column]
        public string Code { get; set; }
        [Column]
        public string PhoneNumber { get; set; }
        [Column]
        public int? TypeLevel { get; set; }
        [Column]
        public string VerifyCode { get; set; }
        [Column]
        public string BankCode { get; set; }
        [Column]
        public string BankName { get; set; }

        [Column]
        public DateTime? CreateDate { get; set; }
    }



    [TableName("Import")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class Import : CreateUpdate
    {
        [Column]
        public int ID { get; set; }

        [Column]
        public int? IDDetail { get; set; }
        [Column]
        public int? Fee { get; set; }
        [Column]
        public long? Total { get; set; }
        [Column]
        public string Note { get; set; }
    }

    [TableName("ImportDetail")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]
    public partial class ImportDetail : CreateUpdate
    {
        [Column]
        public int ID { get; set; }
        [Column]
        public int? IDProduct { get; set; }
        [Column]
        public int? Price { get; set; }
        [Column]
        public int? Number { get; set; }
        [Column]
        public long? Total { get; set; }
    }



    [TableName("Order")]

    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]
    public partial class Order : CreateUpdate
    {
        [Column]
        public int ID { get; set; }
        [Column]
        public int IDGuest { get; set; }
        [Column]
        public int IDDetail { get; set; }
        [Column]
        public string Note { get; set; }
        [Column]
        public int? IDStatus { get; set; }
        [Column]
        public DateTime? DateExpect { get; set; }
        [Column]
        public DateTime? DateActual { get; set; }
    }



    [TableName("OrderDetail")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class OrderDetail : CreateUpdate
    {
        [Column]
        public int ID { get; set; }
        [Column]
        public int IDProduct { get; set; }
        [Column]
        public int Price { get; set; }
        [Column]
        public int Number { get; set; }
        [Column]
        public long? Total { get; set; }
    }

    [TableName("Product")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]
    public partial class Product : CreateUpdate
    {
        [Column]
        public long ID { get; set; }
        [Column]
        public string Code { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string Describe { get; set; }

        [Column]
        public string Body { get; set; }
        [Column]
        public string Color { get; set; }
        [Column]
        public string MetaDetail { get; set; }

        [Column]
        public bool IsPublish { get; set; }
        [Column]
        public bool? IsSale { get; set; }
        [Column("IsNew")]
        public bool? IsNew { get; set; }
        [Column]
        public string ThumbPath { get; set; }

        [Column]
        public string ThumbName { get; set; }

        [Column]
        public byte? IDType { get; set; }
        [Column]
        public int IDCategory { get; set; }
        [Column]
        public string IDEvent { get; set; }
        [Column]
        public int? PriceImport { get; set; }
        [Column]
        public int? PriceRetail { get; set; }
        [Column]
        public int? PriceSale { get; set; }
        [Column]
        public int? PricePromotion { get; set; }
        [Column]
        public int? PercetPromotion { get; set; }
        [Column]
        public string Tags { get; set; }
        [Column]
        public DateTime? Published { get; set; }
        [Column]
        public int? PublishBy { get; set; }

    }
    [TableName("ProductViewed")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class ProductViewed : CreateUpdate
    {
        [Column]
        public int ID { get; set; }
        [Column]
        public int? IDProduct { get; set; }

        [Column]
        public string IP { get; set; }
        [Column]
        public int? IDGuest { get; set; }
    }



    [TableName("SliderInfor")]

    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class SliderInfor : CreateUpdate
    {
        
        [Column]
        public int ID { get; set; }
        [Column]
        public string ThumbPath { get; set; }
        [Column]
        public string ThumbName { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string Describe { get; set; }
        [Column]
        public string Link { get; set; }
        [Column]
        public DateTime? Published { get; set; }
        [Column]
        public int? PublishBy { get; set; }
        [Column]
        public bool IsPublish { get; set; }
    }
    
    [TableName("Status")]
    [PrimaryKey("ID", AutoIncrement = false)]
    [ExplicitColumns]

    public partial class Status : CreateUpdate
    {
        [Column]
        public int ID { get; set; }
        [Column]
        public string Name { get; set; }
    }
    
    [TableName("StgFiles")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [ExplicitColumns]

    public partial class StgFiles : CreateUpdate
    {
        [Column]
        public int ID { get; set; }

        [Column]
        public long IDDoc { get; set; }
        [Column]
        public byte Type { get; set; }
        [Column]
        public string ThumbPath { get; set; }
        [Column]
        public string ThumbName { get; set; }
      
    }
    [TableName("User")]
    [PrimaryKey("ID", AutoIncrement = true)]
    
    [ExplicitColumns]

    public partial class User : CreateUpdate
    {
        [Column]
        public int ID { get; set; }
        [Column]
        public string UserName { get; set; }
        [Column]
        public string FullName { get; set; }
        [Column]
        public string PassWord { get; set; }
        [Column]
        public bool? IsRead { get; set; }
        [Column]
        public bool? IsInsert { get; set; }
        [Column]
        public bool? IsUpdate { get; set; }
        [Column]
        public bool? IsAdmin { get; set; }

        [Column]
        public bool? IsActive { get; set; }
    }


}
