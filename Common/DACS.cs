﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Common.Dac
{
    public static class DACS
    {
        #region ------ Property
        public static string admin
        {
            get
            {
                string crhost = System.Web.HttpContext.Current.Request.Url.Host + "dac";
                return crhost.ToLower().Contains("localhost") ? "" : "/admin";
            }
        } //"/admin"
        public static string stg
        {
            get
            {
                string crhost = System.Web.HttpContext.Current.Request.Url.Host;
                return crhost.ToLower().Contains("localhost") ? "" : "/stg";
            }
        }//"/stg"
        public enum HashType
        {
            Md5,
            Sha1,
            Sha256,
            Sha512
        }
        private static List<string> Hosts
        {
            get
            {
                return new List<string> { "phukiensieuxinh.com", "localhost" };
            }
        }
        private static List<string> Area
        {
            get
            {
                return new List<string> { "admin", "stg" };
            }
        }
        #endregion
        public static string T(string input,params object[] prs)
        {
            return string.Format( input,prs);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            try
            {
                byte[] bytes = Convert.FromBase64String(base64EncodedData);
                return Encoding.UTF8.GetString(bytes);
            }
            catch
            {
                return null;
            }
        }
        public static string Base64Encode(string plainText)
        {
            try
            {
                return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
            }
            catch
            {
                return null;
            }
        }

        public static T Bind<T>(Hashtable data)
        {
            return ((T)Activator.CreateInstance(typeof(T))).Bind<T>(data, 0, 0);
        }
        public static T BindCreate<T>(Hashtable data, int iduser)
        {
            return ((T)Activator.CreateInstance(typeof(T))).Bind<T>(data, iduser, 0);
        }
        public static T BindCreate<T>(this T data, int iduser)
        {
            if (!object.Equals(data, null))
            {
                foreach (var info in data.GetType().GetProperties())
                {
                    Type propertyType = info.PropertyType;
                    if (((propertyType == typeof(int)) || (propertyType == typeof(int?))) && (info.Name == "CreatedBy"))
                    {
                        info.SetValue(data, iduser);
                    }
                    if (((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?))) && (info.Name == "Created"))
                    {
                        info.SetValue(data, DateTime.Now);
                    }
                }
            }
            return data;
        }
        public static T BindUpdate<T>(this T data, int iduser)
        {
            if (!object.Equals(data, null))
            {
                foreach (var info in data.GetType().GetProperties())
                {
                    Type propertyType = info.PropertyType;
                    if (((propertyType == typeof(int)) || (propertyType == typeof(int?))) && (info.Name == "UpdatedBy"))
                    {
                        info.SetValue(data, iduser);
                    }
                    if (((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?))) && (info.Name == "Updated"))
                    {
                        info.SetValue(data, DateTime.Now);
                    }
                }
            }
            return data;
        }
        public static T BindUpdate<T>(Hashtable data, int iduser)
        {
            return ((T)Activator.CreateInstance(typeof(T))).Bind<T>(data, 0, iduser);
        }

        public static T Bind<T>(this T obj, Hashtable data, int createdBy = 0, int updatedBy = 0)
        {
            if (!object.Equals(data, null))
            {
                foreach (PropertyInfo info in obj.GetType().GetProperties())
                {
                    try
                    {
                        DateTime? nullable;
                        Type propertyType = info.PropertyType;
                        if (createdBy > 0)
                        {
                            if (((propertyType == typeof(int)) || (propertyType == typeof(int?))) && (info.Name == "CreatedBy"))
                            {
                                info.SetValue(obj, createdBy);
                            }
                            if (((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?))) && (info.Name == "Created"))
                            {
                                info.SetValue(obj, DateTime.Now);
                            }
                        }
                        if (updatedBy > 0)
                        {
                            if (((propertyType == typeof(int)) || (propertyType == typeof(int?))) && (info.Name == "UpdatedBy"))
                            {
                                info.SetValue(obj, updatedBy);
                            }
                            if (((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?))) && (info.Name == "Updated"))
                            {
                                info.SetValue(obj, DateTime.Now);
                            }
                        }
                        if (data.ContainsKey(info.Name))
                        {
                            if (propertyType == typeof(string))
                            {
                                string str = GetString(data, info.Name, false);
                                info.SetValue(obj, str);
                            }
                            if (propertyType == typeof(string[]))
                            {
                                string[] strings = GetStrings(data, info.Name);
                                info.SetValue(obj, strings);
                            }
                            else if ((propertyType == typeof(long)) || (propertyType == typeof(long?)))
                            {
                                long bigInt = GetBigInt(data, info.Name);
                                info.SetValue(obj, bigInt);
                            }
                            else if (propertyType == typeof(long[]))
                            {
                                long[] bigInts = GetBigInts(data, info.Name);
                                info.SetValue(obj, bigInts);
                            }
                            else if ((propertyType == typeof(int)) || (propertyType == typeof(int?)))
                            {
                                int @int = GetInt(data, info.Name);
                                info.SetValue(obj, @int);
                            }
                            else if ((propertyType == typeof(byte)) || (propertyType == typeof(byte?)))
                            {
                                byte tinyint = GetTinyint(data, info.Name);
                                info.SetValue(obj, tinyint);
                            }
                            else if (propertyType == typeof(int[]))
                            {
                                int[] ints = GetInts(data, info.Name);
                                info.SetValue(obj, ints);
                            }
                            else if (propertyType == typeof(byte[]))
                            {
                                byte[] tinyints = GetTinyints(data, info.Name);
                                info.SetValue(obj, tinyints);
                            }
                            else if ((propertyType == typeof(bool)) || (propertyType == typeof(bool?)))
                            {
                                bool @bool = GetBool(data, info.Name);
                                info.SetValue(obj, @bool);
                            }
                            else if ((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?)))
                            {
                                nullable = GetDatetime(data, info.Name, "dd-MM-yyyy HH:mm");
                                if (!object.Equals(nullable, null))
                                {
                                    DateTime? nullable2 = nullable;
                                    DateTime minValue = DateTime.MinValue;
                                    if (!(nullable2.HasValue ? (nullable2.GetValueOrDefault() <= minValue) : false))
                                    {
                                        DateTime? nullable3 = nullable;
                                        DateTime maxValue = DateTime.MaxValue;
                                        if (!(nullable3.HasValue ? (nullable3.GetValueOrDefault() >= maxValue) : false))
                                        {
                                            goto Label_0462;
                                        }
                                    }
                                }
                                info.SetValue(obj, null);
                            }
                        }
                        continue;
                        Label_0462:
                        info.SetValue(obj, nullable);
                    }
                    catch
                    {
                    }
                }
            }
            return obj;
        }

        public static long ByteToGb(long bytes)
        {
            if (bytes <= 0L)
            {
                return 0L;
            }
            return (((bytes / 0x400L) / 0x400L) / 0x400L);
        }

        public static T CopyTo<T>(object source)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source));
            }
            catch
            {
                return default(T);
            }
        }

        public static T CopySqlToOracle<T>(this T obj, object data)
        {
            if (!object.Equals(data, null))
            {
                foreach (PropertyInfo info in obj.GetType().GetProperties())
                {
                    try
                    {
                        Type propertyType = info.PropertyType;
                        var value = GetValueOfObject(data, info.Name);
                        info.SetValue(obj, value);
                    }
                    catch
                    {
                    }
                }
            }
            return obj;
        }
        public static object GetValueOfObject(object data, string name)
        {
            foreach (var info in data.GetType().GetProperties())
            {
                if (info.Name.ToUpper() == name.ToUpper())
                {
                    if (info.GetType() == typeof(bool) || info.GetType() == typeof(bool?))
                    {
                        if (info.GetValue(data) == null)
                        {
                            return null;
                        }
                        else if ((bool)info.GetValue(data) == true)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return info.GetValue(data);
                    }
                }
            }
            return null;
        }

        public static bool CreateDir(string path)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
                return true;
            }
            catch
            {
                return IsDirExists(path);
            }
        }
        public static bool IsDirExists(string path)
        {
            try
            {
                return Directory.Exists(Path.GetDirectoryName(path));
            }
            catch
            {
                return false;
            }
        }




        public static bool Crop(string sourceFile, string destFile, int w, int h, int x, int y)
        {
            bool flag;
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (Image image = Image.FromFile(sourceFile))
                    {
                        using (Bitmap bitmap = new Bitmap(w, h))
                        {
                            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                            using (Graphics graphics = Graphics.FromImage(bitmap))
                            {
                                graphics.SmoothingMode = SmoothingMode.HighQuality;
                                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                                graphics.DrawImage(image, new Rectangle(0, 0, w, h), x, y, w, h, GraphicsUnit.Pixel);
                                bitmap.Save(stream, image.RawFormat);
                            }
                        }
                    }
                    string directoryName = Path.GetDirectoryName(destFile);
                    if (directoryName != null)
                    {
                        Directory.CreateDirectory(directoryName);
                    }
                    new Bitmap(stream).Save(destFile, ImageFormat.Jpeg);
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                flag = false;
            }
            return flag;
        }


        public static string GeneratePassword(bool isHash = false, int length = 6)
        {
            string password = Membership.GeneratePassword(length, 2);
            if (isHash)
            {
                password = GetPasswordHash(password);
            }
            return password;
        }
        public static string GetPasswordHash(string password)
        {
            return GetSha1HashData(password);
        }
        private static string GetSha1HashData(string data)
        {
            byte[] buffer = SHA1.Create().ComputeHash(Encoding.Default.GetBytes(data));
            StringBuilder builder = new StringBuilder();
            foreach (byte num in buffer)
            {
                builder.Append(num.ToString());
            }
            return builder.ToString();
        }
        public static long GetBigInt(Hashtable args, object i)
        {
            try
            {
                return long.Parse(args[i].ToString());
            }
            catch
            {
                return 0L;
            }
        }

        public static long[] GetBigInts(Hashtable args, object i)
        {
            try
            {
                object obj2 = args[i];
                if (obj2.GetType().ToString() == "Newtonsoft.Json.Linq.JArray")
                {
                    return JsonConvert.DeserializeObject<List<long>>(obj2.ToString()).ToArray();
                }
                if (IsNumber(obj2.ToString()))
                {
                    return new long[] { long.Parse(obj2.ToString()) };
                }
                if (obj2.GetType().ToString() == "System.String")
                {
                    try
                    {
                        long[] objA = JsonConvert.DeserializeObject<List<long>>(obj2.ToString()).ToArray();
                        if (!object.Equals(objA, null) && objA.Any<long>())
                        {
                            return objA;
                        }
                    }
                    catch
                    {
                    }
                    return (from x in obj2.ToString().Split(new char[] { ',' }) select long.Parse(x)).ToArray<long>();
                }
                return (long[])obj2;
            }
            catch
            {
                return null;
            }
        }
        public static bool IsNumber(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }
            foreach (char ch in str.Trim().ToCharArray())
            {
                if (!char.IsDigit(ch))
                {
                    return false;
                }
            }
            return true;
        }
        public static bool GetBool(Hashtable args, object i)
        {
            try
            {
                switch (args[i].ToString().Trim().ToLowerInvariant())
                {
                    case "true":
                        return true;

                    case "t":
                        return true;

                    case "1":
                        return true;

                    case "yes":
                        return true;

                    case "y":
                        return true;

                    case "on":
                        return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static Hashtable GetDataPost()
        {
            Hashtable hashtable = new Hashtable();
            try
            {
                HttpRequest request = HttpContext.Current.Request;
                foreach (string str in request.Form.AllKeys)
                {
                    try
                    {
                        hashtable.Add(str, request.Unvalidated.Form[str]);
                    }
                    catch
                    {
                    }
                }
                foreach (string str2 in request.QueryString.AllKeys)
                {
                    if (!hashtable.ContainsKey(str2))
                    {
                        try
                        {
                            hashtable.Add(str2, request.Unvalidated.QueryString[str2]);
                        }
                        catch
                        {
                        }
                    }
                }
            }
            catch
            {
            }
            return hashtable;
        }

        public static DateTime? GetDate(string dateString, string format = "dd-MM-yyyy")
        {
            try
            {
                return new DateTime?(DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture));
            }
            catch
            {
                return null;
            }
        }
        public static DateTime? GetDate(Hashtable args, object i, string format = "dd-MM-yyyy")
        {
            try
            {
                if ((args[i] is DateTime) || (args[i] is DateTime?))
                {
                    return new DateTime?((DateTime)args[i]);
                }
                return new DateTime?(DateTime.ParseExact(args[i].ToString(), format, CultureInfo.InvariantCulture));
            }
            catch
            {
                return null;
            }
        }
        public static DateTime? GetDatetime(string dateString, string format = "dd-MM-yyyy HH:mm")
        {
            try
            {
                return new DateTime?(DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture));
            }
            catch
            {
                return null;
            }
        }
        public static DateTime? GetDatetime(Hashtable args, object i, string format = "dd-MM-yyyy HH:mm")
        {
            try
            {
                if ((args[i] is DateTime) || (args[i] is DateTime?))
                {
                    return new DateTime?((DateTime)args[i]);
                }
                return new DateTime?(DateTime.ParseExact(args[i].ToString(), format, CultureInfo.InvariantCulture));
            }
            catch
            {
                return GetDate(GetString(args, i, false), "dd-MM-yyyy");
            }
        }

        public static string GetString(Hashtable args, object i, bool removeSpace = false)
        {
            try
            {
                if (removeSpace)
                {
                    return args[i].ToString().Replace(" ", "").Trim();
                }
                return args[i].ToString().Trim(new char[] { ' ', '\t' });
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string GetFieldStrings<T>(string tableName, List<string> expts = null)
        {
            if (object.Equals(expts, null))
            {
                expts = new List<string>();
            }
            List<string> values = new List<string>();
            T local = (T)Activator.CreateInstance(typeof(T));
            foreach (PropertyInfo info in local.GetType().GetProperties())
            {
                if (!expts.Contains(info.Name))
                {
                    values.Add(string.Format("[{0}].[{1}]", tableName, info.Name));
                }
            }
            return string.Join(", ", values);
        }

        public static string GetHash(string text, HashType hashType)
        {
            switch (hashType)
            {
                case HashType.Md5:
                    return GetMd5(text);

                case HashType.Sha1:
                    return GetSha1(text);

                case HashType.Sha256:
                    return GetSha256(text);

                case HashType.Sha512:
                    return GetSha512(text);
            }
            return "Invalid Hash Type";
        }
        private static string GetMd5(string text)
        {
            byte[] bytes = new UnicodeEncoding().GetBytes(text);
            using (MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider())
            {
                return provider.ComputeHash(bytes).Aggregate<byte, string>("", (current, x) => (current + string.Format("{0:x2}", x)));
            }
        }

        private static string GetSha1(string text)
        {
            byte[] bytes = new UnicodeEncoding().GetBytes(text);
            using (SHA1Managed managed = new SHA1Managed())
            {
                return managed.ComputeHash(bytes).Aggregate<byte, string>("", (current, x) => (current + string.Format("{0:x2}", x)));
            }
        }

        private static string GetSha256(string text)
        {
            byte[] bytes = new UnicodeEncoding().GetBytes(text);
            using (SHA256Managed managed = new SHA256Managed())
            {
                return managed.ComputeHash(bytes).Aggregate<byte, string>("", (current, x) => (current + string.Format("{0:x2}", x)));
            }
        }

        private static string GetSha512(string text)
        {
            byte[] bytes = new UnicodeEncoding().GetBytes(text);
            using (SHA512Managed managed = new SHA512Managed())
            {
                return managed.ComputeHash(bytes).Aggregate<byte, string>("", (current, x) => (current + string.Format("{0:x2}", x)));
            }
        }

        public static int GetInt(Hashtable args, object i)
        {
            try
            {
                return int.Parse(args[i].ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static int[] GetInts(List<string> parentStrs)
        {
            List<int> source = new List<int>();
            try
            {
                foreach (string str in parentStrs)
                {
                    string[] strArray = str.Trim(new char[] { '|' }).Split(new char[] { '|' });
                    source.AddRange((from x in strArray select int.Parse(x)).ToList<int>());
                }
            }
            catch
            {
            }
            return source.Distinct<int>().ToArray<int>();
        }

        public static int[] GetInts(Hashtable args, object i)
        {
            try
            {
                object obj2 = args[i];
                if (obj2.GetType().ToString() == "Newtonsoft.Json.Linq.JArray")
                {
                    return JsonConvert.DeserializeObject<List<int>>(obj2.ToString()).ToArray();
                }
                if (IsNumber(obj2.ToString()))
                {
                    return new int[] { int.Parse(obj2.ToString()) };
                }
                if (obj2.GetType().ToString() == "System.String")
                {
                    try
                    {
                        int[] objA = JsonConvert.DeserializeObject<List<int>>(obj2.ToString()).ToArray();
                        if (!object.Equals(objA, null) && objA.Any<int>())
                        {
                            return objA;
                        }
                    }
                    catch
                    {
                    }
                    return (from x in obj2.ToString().Split(new char[] { ',' }) select int.Parse(x)).ToArray<int>();
                }
                return (int[])obj2;
            }
            catch
            {
                return null;
            }
        }

        public static long? GetNulableBigInt(Hashtable args, object i)
        {
            try
            {
                if (string.IsNullOrEmpty(args[i].ToString()))
                {
                    return null;
                }
                return new long?(long.Parse(args[i].ToString()));
            }
            catch
            {
                return null;
            }
        }

        public static T GetObjects<T>(Hashtable args, object i)
        {
            try
            {
                object obj2 = args[i];
                if (obj2.GetType().ToString() == "Newtonsoft.Json.Linq.JArray")
                {
                    return JsonConvert.DeserializeObject<T>(obj2.ToString());
                }
                return (T)obj2;
            }
            catch
            {
                return default(T);
            }
        }

        public static string GetSavePath(string name, out string absPath, string StgDirFile)
        {
            string fullPathPdf = string.Empty;
            return GetSavePath(name, out absPath, out fullPathPdf, StgDirFile);
        }

        public static string GetSavePath(string name, out string absPath, out string fullPathPdf, string StgDirFile)
        {
            string extension = Path.GetExtension(name);
            string str2 = DateTime.Now.ToString("yyyy");
            string str3 = DateTime.Now.ToString("MM");
            string str4 = DateTime.Now.ToString("dd");
            string str5 = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            string hash = GetHash(name, HashType.Md5);
            string str7 = string.Format("{0}-{1}{2}", str5, hash, extension);
            string str8 = string.Format("{0}-{1}{2}", str5, hash, ".pdf");
            absPath = string.Format("{0}/{1}/{2}/{3}", new object[] { str2, str3, str4, str7 });
            fullPathPdf = Path.Combine(new string[] { StgDirFile, str2, str3, str4, str8 });
            return Path.Combine(new string[] { StgDirFile, str2, str3, str4, str7 });
        }

        public static bool IsFileExists(string fileName)
        {
            try
            {
                return File.Exists(fileName);
            }
            catch
            {
                return false;
            }
        }


        public static bool IsFolder(string extension)
        {
            return (extension.ToLowerInvariant().Trim(new char[] { '.', ' ' }) == "folder");
        }

        public static bool IsGif(string extension)
        {
            return (extension.ToLowerInvariant().Trim(new char[] { '.', ' ' }) == "gif");
        }

        public static bool IsImage(string extension)
        {
            extension = extension.ToLowerInvariant().Trim(new char[] { '.', ' ' });
            switch (extension)
            {
                case "ico":
                    return true;

                case "bmp":
                    return true;

                case "gif":
                    return true;

                case "jpe":
                    return true;

                case "jpeg":
                    return true;

                case "jpg":
                    return true;

                case "png":
                    return true;
            }
            return false;
        }
        public static bool IsMatch(string password, string hash)
        {
            password = GetPasswordHash(password);
            return (string.CompareOrdinal(password, hash) == 0);
        }

        public static bool IsOffice(string extension)
        {
            extension = extension.ToLowerInvariant().Trim(new char[] { '.', ' ' });
            switch (extension)
            {
                case "rtf":
                    return true;

                case "odt":
                    return true;

                case "doc":
                    return true;

                case "dot":
                    return true;

                case "docx":
                    return true;

                case "dotx":
                    return true;

                case "docm":
                    return true;

                case "dotm":
                    return true;

                case "csv":
                    return true;

                case "odc":
                    return true;

                case "xls":
                    return true;

                case "xlsx":
                    return true;

                case "xlsm":
                    return true;

                case "odp":
                    return true;

                case "ppt":
                    return true;

                case "pptx":
                    return true;

                case "pptm":
                    return true;

                case "pot":
                    return true;

                case "potm":
                    return true;

                case "potx":
                    return true;

                case "pps":
                    return true;

                case "ppsx":
                    return true;

                case "ppsm":
                    return true;
            }
            return false;
        }

        public static bool IsPdf(string extension)
        {
            extension = extension.ToLowerInvariant().Trim(new char[] { '.', ' ' });
            return (extension == "pdf");
        }
        public static bool IsPdfOrOffice(string extension)
        {
            if (!IsPdf(extension))
            {
                return IsOffice(extension);
            }
            return true;
        }

        public static bool IsText(string extension)
        {
            return (extension.ToLowerInvariant().Trim(new char[] { '.', ' ' }) == "txt");
        }

        public static bool IsVideo(string extension)
        {
            extension = extension.ToLowerInvariant().Trim(new char[] { '.', ' ' });
            switch (extension)
            {
                case "3g2":
                    return true;

                case "3gp":
                    return true;

                case "3gp2":
                    return true;

                case "3gpp":
                    return true;

                case "avi":
                    return true;

                case "asf":
                    return true;

                case "asr":
                    return true;

                case "asx":
                    return true;

                case "dif":
                    return true;

                case "dv":
                    return true;

                case "ivf":
                    return true;

                case "flv":
                    return true;

                case "m4v":
                    return true;

                case "lsf":
                    return true;

                case "lsx":
                    return true;

                case "m1v":
                    return true;

                case "m2t":
                    return true;

                case "m2ts":
                    return true;

                case "m2v":
                    return true;

                case "mod":
                    return true;

                case "mov":
                    return true;

                case "movie":
                    return true;

                case "mp2":
                    return true;

                case "mp2v":
                    return true;

                case "mp4":
                    return true;

                case "mp4v":
                    return true;

                case "mpeg":
                    return true;

                case "mpe":
                    return true;

                case "mpa":
                    return true;

                case "mpg":
                    return true;

                case "mpv2":
                    return true;

                case "mqv":
                    return true;

                case "mts":
                    return true;

                case "nsc":
                    return true;

                case "qt":
                    return true;

                case "ts":
                    return true;

                case "tts":
                    return true;

                case "vbk":
                    return true;

                case "wm":
                    return true;

                case "wmp":
                    return true;

                case "wmv":
                    return true;

                case "wmx":
                    return true;

                case "wvx":
                    return true;
            }
            return false;
        }
        public static string JoinGroups(List<string> groups, string op = "OR")
        {
            if (groups.IsEmpty<string>())
            {
                return null;
            }
            op = string.Format(" {0} ", op);
            return string.Format("({0})", string.Join(op, groups));
        }

        public static Hashtable KeyValue<T>(this T obj)
        {
            Hashtable hashtable = new Hashtable();
            foreach (PropertyInfo info in obj.GetType().GetProperties())
            {
                hashtable.Add(info.Name, info.GetValue(obj, null));
            }
            return hashtable;
        }

        public static bool IsEmpty<T>(this IEnumerable<T> data)
        {
            return ((data == null) || !data.Any<T>());
        }

        public static bool IsEmpty(object obj)
        {
            if (obj is string)
            {
                return string.IsNullOrEmpty((string)obj);
            }
            return object.Equals(obj, null);
        }


        public static long[] ParserBigInts(string parentStr, char c = '|')
        {
            List<long> source = new List<long>();
            try
            {
                string[] strArray = parentStr.Trim(new char[] { c }).Split(new char[] { c });
                source.AddRange((from x in strArray select long.Parse(x)).ToList<long>());
            }
            catch
            {
            }
            return source.Distinct<long>().ToArray<long>();
        }
        public static int[] ParserInts(string parentStr, char c = '|')
        {
            List<int> source = new List<int>();
            try
            {
                string[] strArray = parentStr.Trim(new char[] { c }).Split(new char[] { c });
                source.AddRange((from x in strArray select int.Parse(x)).ToList<int>());
            }
            catch
            {
            }
            return source.Distinct<int>().ToArray<int>();
        }
        public static JsonResult Render403()
        {
            return new JsonResult { Data = new { isDL = 1, wDL = 600, htDL = "<div title='" + ("Giới hạn truy cập") + "' id='dialogPage403' class='text-center'>\r\n                                <img src='/Assets/app/img/page403.png' class='responsite' />\r\n                            </div>" } };
        }
        public static string RenderAudio(string path)
        {
            return string.Join(string.Empty, new string[] { "<div class='doc-viewer media-viewer'>\r\n                    <input value='1' type='hidden' id='pageNumber' />\r\n                    <div id='textLayer1' class='media-contain'><div id='Player' class='media-player' data-video='0' data-value='", path, "'></div></div>\r\n                </div>" });
        }

        /// <summary>
        /// tự sinh ra Thẻ option trong thẻ select
        /// </summary>
        /// <param name="lst"> list dữ liệu đầu vào</param>
        /// <param name="selected"> item được   chọn </param>
        /// <param name="all">Có cho phép hiện thị item tât cả không</param>
        /// <param name="txtall">Nội dung hiện thị của item tất cả nếu có</param>
        /// <returns></returns>
        public static string RenderOptions(dynamic lst, int selected = 0, bool all = true, string txtall = "")
        {
            StringBuilder str = new StringBuilder();
            string option = "";
            if (all)
            {
                option = string.Format("<option data-target='{0}' selected data-url='{1}' data-parent='{2}'  value='{3}'>{4}</option>", "", "", 0, 0, txtall);
                str.Append(option);
            }
            foreach (var item in lst)
            {
                long value = item.ID;
                string name = item.Name;

                if (value == selected)
                    option = string.Format("<option data-target='{0}' selected data-url='{1}' data-parent='{2}'  value='{3}'>{4}</option>", "", "", item.Parent, value, name);
                else
                    option = string.Format("<option data-target='{0}' data-url='{1}' data-parent='{2}'  value='{3}'>{4}</option>", "", "", item.Parent, value, name);
                str.Append(option);
            }
            return str.ToString();
        }
        public static string RenderOptionGroups(dynamic datas, long selected = 0, bool all = true, string txtall = "")
        {
            List<OptionModel> lst = new List<OptionModel>();
            foreach (var item in datas)
            {
                lst.Add(new OptionModel() { ID = item.ID, Name = item.Name, Parent = item.Parent });
            }
            StringBuilder str = new StringBuilder();
            string option = "";
            var parents = lst.Where(t => t.Parent == 0).ToList();
            if (all)
            {
                option = string.Format("<option data-target='{0}' data-url='{1}' data-parent='{2}'  value='{3}'>{4}</option>", "", "", 0, 0, txtall);
                str.Append(option);
            }
            if (parents.Any())
            {
                foreach (var item in parents)
                {
                    str.Append(string.Format("<optgroup label='{0}'>", item.Name));
                    var childs = lst.Where(t => t.Parent == item.ID).ToList();
                    if (childs.Any())
                    {
                        foreach (var it in childs)
                        {
                            if (it.ID == selected)
                                option = string.Format("<option  selected  data-parent='{0}'  value='{1}'>{2}</option>", item.ID, it.ID, it.Name);
                            else
                                option = string.Format("<option   data-parent='{0}'  value='{1}'>{2}</option>", item.ID, it.ID, it.Name);
                            str.Append(option);
                        }
                    }
                    str.Append("</optgroup>");
                }
            }
            return str.ToString();
        }

        public static string RenderUnk()
        {
            return string.Join(string.Empty, new string[] { "<div class='doc-viewer'>\r\n                    <input value='1' type='hidden' id='pageNumber' />\r\n                    <div id='textLayer1' class='media-contain'><div>", ("Dạng t\x00e0i liệu kh\x00f4ng hỗ trợ xem tr\x00ean web"), "</div></div>\r\n                </div>" });
        }
        public static string UrlGetDomain(Uri url)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(url.Scheme);
            builder.Append("://");
            builder.Append(url.Host);
            int port = url.Port;
            if ((port != 80) && (port != 0x1bb))
            {
                builder.Append(":");
                builder.Append(port);
            }
            return builder.ToString();
        }

        public static string UrlSetParam(string url, string name, string data)
        {
            try
            {
                Uri uri = new Uri(url);
                StringBuilder builder = new StringBuilder();
                builder.Append(uri.Scheme);
                builder.Append("://");
                builder.Append(uri.Host);
                int port = uri.Port;
                if (port != 80)
                {
                    builder.Append(":");
                    builder.Append(port);
                }
                if (!string.IsNullOrEmpty(uri.AbsolutePath))
                {
                    builder.Append(uri.AbsolutePath);
                }
                builder.Append("?");
                if (!string.IsNullOrEmpty(uri.Query))
                {
                    builder.Append(uri.Query.Trim(new char[] { '?', '&' }));
                    builder.Append("&");
                }
                builder.Append(string.Join("=", new string[] { name, data }));
                return builder.ToString();
            }
            catch
            {
                return url;
            }
        }

        public static string IpAddress
        {
            get
            {
                try
                {
                    HttpRequest request = HttpContext.Current.Request;
                    string str = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrEmpty(str))
                    {
                        str = request.ServerVariables["REMOTE_ADDR"] ?? request.UserHostAddress;

                    }
                    return str;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }
        public static bool TryGetHost(ref string host, HttpRequest request = null)
        {
            try
            {
                if (object.Equals(request, null))
                {
                    request = HttpContext.Current.Request;
                }
                string str = request.ServerVariables["SERVER_PORT"];
                str = (((str == null) || (str == "80")) || (str == "443")) ? string.Empty : string.Format(":{0}", str);
                string str2 = request.ServerVariables["SERVER_PORT_SECURE"];
                str2 = ((str2 == null) || (str2 == "0")) ? "http://" : "https://";
                string str3 = request.ServerVariables["SERVER_NAME"];
                host = string.Format("{0}{1}{2}", str2, str3, str);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string TryGetParam(Uri uri, string key)
        {
            try
            {
                return HttpUtility.ParseQueryString(uri.Query).Get(key);
            }
            catch
            {
                return string.Empty;
            }
        }


        public static bool TryGetValue(string[] args, int i, ref int result)
        {
            try
            {
                result = int.Parse(args.GetValue(i).ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool TryGetValue(string[] args, int i, ref long result)
        {
            try
            {
                result = long.Parse(args.GetValue(i).ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool TryGetValue(string[] args, int i, ref string result)
        {
            try
            {
                result = args.GetValue(i).ToString();
                return true;
            }
            catch
            {
                return false;
            }
        }


        private static string UppercaseWords(string str)
        {
            List<string> values = new List<string>();
            try
            {
                foreach (string str2 in str.ToLowerInvariant().Split(new char[] { '-', ' ' }))
                {
                    char[] chArray = str2.ToCharArray();
                    chArray[0] = char.ToUpper(chArray[0]);
                    values.Add(new string(chArray));
                }
            }
            catch
            {
            }
            return string.Join(string.Empty, values);
        }

        public static string WSCleaner(string dataStr)
        {
            try
            {
                string str = Regex.Replace(Regex.Replace(Regex.Replace(dataStr, @"\s*\n\s*", "\n"), @"\s*\>\s*\<\s*", "><"), "<!--(.*?)-->", "");
                int index = str.IndexOf(">");
                if (index >= 0)
                {
                    str = str.Remove(index, 1).Insert(index, ">");
                }
                return str;
            }
            catch
            {
                return dataStr;
            }
        }
        public static string[] GetStrings(Hashtable args, object i, char separator)
        {
            try
            {
                object obj2 = args[i];
                return obj2.ToString().Split(new char[] { separator });
            }
            catch
            {
                return null;
            }
        }
        public static string[] GetStrings(Hashtable args, object i)
        {
            try
            {
                object obj2 = args[i];
                if (obj2.GetType().ToString() == "Newtonsoft.Json.Linq.JArray")
                {
                    return JsonConvert.DeserializeObject<List<string>>(obj2.ToString()).ToArray();
                }
                if (obj2.GetType().ToString() == "System.String")
                {
                    try
                    {
                        string[] objA = JsonConvert.DeserializeObject<List<string>>(obj2.ToString()).ToArray();
                        if (!object.Equals(objA, null) && objA.Any<string>())
                        {
                            return objA;
                        }
                    }
                    catch
                    {
                    }
                    return obj2.ToString().Split(new char[] { ',' });
                }
                return (string[])obj2;
            }
            catch
            {
                return null;
            }
        }
        public static byte GetTinyint(Hashtable args, object i)
        {
            try
            {
                return byte.Parse(args[i].ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static byte[] GetTinyints(Hashtable args, object i)
        {
            try
            {
                object obj2 = args[i];
                if (obj2.GetType().ToString() == "Newtonsoft.Json.Linq.JArray")
                {
                    return JsonConvert.DeserializeObject<List<byte>>(obj2.ToString()).ToArray();
                }
                if (IsNumber(obj2.ToString()))
                {
                    return new byte[] { byte.Parse(obj2.ToString()) };
                }
                if (obj2.GetType().ToString() == "System.String")
                {
                    try
                    {
                        byte[] objA = JsonConvert.DeserializeObject<List<byte>>(obj2.ToString()).ToArray();
                        if (!object.Equals(objA, null) && objA.Any<byte>())
                        {
                            return objA;
                        }
                    }
                    catch
                    {
                    }
                    return (from x in obj2.ToString().Split(new char[] { ',' }) select byte.Parse(x)).ToArray<byte>();
                }
                return (byte[])obj2;
            }
            catch
            {
                return null;
            }
        }

        public static bool HasValue<T>(this T obj)
        {
            foreach (PropertyInfo info in obj.GetType().GetProperties())
            {
                try
                {
                    Type propertyType = info.PropertyType;
                    if ((propertyType == typeof(string)) && !string.IsNullOrEmpty(info.GetValue(obj).ToString()))
                    {
                        return true;
                    }
                    if (propertyType == typeof(string[]))
                    {
                        string[] data = (string[])info.GetValue(obj);
                        if (!data.IsEmpty<string>())
                        {
                            return true;
                        }
                    }
                    else if ((propertyType == typeof(long)) || (propertyType == typeof(long?)))
                    {
                        long num = (long)info.GetValue(obj);
                        if (num > 0L)
                        {
                            return true;
                        }
                    }
                    else if (propertyType == typeof(long[]))
                    {
                        long[] numArray = (long[])info.GetValue(obj);
                        if (!numArray.IsEmpty<long>())
                        {
                            return true;
                        }
                    }
                    else if ((propertyType == typeof(int)) || (propertyType == typeof(int?)))
                    {
                        int num2 = (int)info.GetValue(obj);
                        if (num2 > 0)
                        {
                            return true;
                        }
                    }
                    else if ((propertyType == typeof(byte)) || (propertyType == typeof(byte?)))
                    {
                        byte num3 = (byte)info.GetValue(obj);
                        if (num3 > 0)
                        {
                            return true;
                        }
                    }
                    else if (propertyType == typeof(int[]))
                    {
                        int[] numArray2 = (int[])info.GetValue(obj);
                        if (!numArray2.IsEmpty<int>())
                        {
                            return true;
                        }
                    }
                    else if (propertyType == typeof(byte[]))
                    {
                        byte[] buffer = (byte[])info.GetValue(obj);
                        if (!buffer.IsEmpty<byte>())
                        {
                            return true;
                        }
                    }
                    else if ((propertyType == typeof(bool)) || (propertyType == typeof(bool?)))
                    {
                        if ((bool)info.GetValue(obj))
                        {
                            return true;
                        }
                    }
                    else if ((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?)))
                    {
                        DateTime time = (DateTime)info.GetValue(obj);
                        if (IsDate(new DateTime?(time)))
                        {
                            return true;
                        }
                    }
                }
                catch
                {
                }
            }
            return false;
        }

        public static bool IsDate(DateTime? dt)
        {
            if (!dt.HasValue)
            {
                return false;
            }
            DateTime? nullable = dt;
            DateTime minValue = DateTime.MinValue;
            if (nullable.HasValue)
            {
                return (nullable.GetValueOrDefault() != minValue);
            }
            return true;
        }

        public static string Summary(string dataString, int wordCount = 50)
        {
            if (string.IsNullOrEmpty(dataString))
            {
                return dataString;
            }
            int num = 0;
            string str = dataString.Normalize(NormalizationForm.FormD);
            StringBuilder builder = new StringBuilder();
            foreach (char ch in str)
            {
                if (char.IsWhiteSpace(ch))
                {
                    num++;
                    if (num <= wordCount)
                    {
                        builder.Append(ch);
                        continue;
                    }
                    builder.Append(ch);
                    builder.Append('.');
                    builder.Append('.');
                    builder.Append('.');
                    break;
                }
                builder.Append(ch);
            }
            return builder.ToString();
        }

        public static string Summarychar(string dataString, int charCount = 50)
        {
            if (string.IsNullOrEmpty(dataString))
            {
                return dataString;
            }
            int num = 0;
            string str = dataString.Normalize(NormalizationForm.FormD);
            StringBuilder builder = new StringBuilder();
            foreach (char ch in str)
            {
                num++;
                if (num <= charCount)
                {
                    builder.Append(ch);
                }
                else
                {
                    builder.Append(ch);
                    builder.Append('.');
                    builder.Append('.');
                    builder.Append('.');
                    break;
                }
            }
            return builder.ToString();
        }


        public static string SummaryName(string dataString, int beforeWC = 2, int afterWC = 2)
        {
            if (string.IsNullOrEmpty(dataString))
            {
                return dataString;
            }
            string str = dataString.Normalize(NormalizationForm.FormD);
            StringBuilder builder = new StringBuilder();
            List<string> source = new List<string>();
            foreach (char ch in str)
            {
                if (char.IsWhiteSpace(ch))
                {
                    source.Add(builder.ToString());
                    builder = new StringBuilder();
                }
                else
                {
                    builder.Append(ch);
                }
            }
            source.Add(builder.ToString());
            try
            {
                int count = source.Count - afterWC;
                if (count < beforeWC)
                {
                    count = beforeWC + 1;
                }
                string str2 = string.Join(" ", source.Take<string>(beforeWC).ToList<string>());
                string str3 = string.Join(" ", source.Skip<string>(count).Take<string>(afterWC).ToList<string>());
                return string.Join(" ... ", new string[] { str2, str3 });
            }
            catch
            {
                return dataString;
            }
        }
        public static string TrimEnd(this string strSource, string strSuffix, bool ignoreCase = false)
        {
            try
            {
                if (object.Equals(strSource, null))
                {
                    return strSource;
                }
                if (object.Equals(strSuffix, null))
                {
                    return strSource;
                }
                if (!ignoreCase)
                {
                    if (!strSource.EndsWith(strSuffix))
                    {
                        return strSource;
                    }
                }
                else
                {
                    string str = strSource.ToLowerInvariant();
                    string str2 = strSuffix.ToLowerInvariant();
                    if (!str.EndsWith(str2))
                    {
                        return strSource;
                    }
                }
                return strSource.Substring(0, strSource.Length - strSuffix.Length);
            }
            catch
            {
                return strSource;
            }
        }
        public static void Rewrite(HttpRequest Request, HttpContext Context)
        {
            string input = Request.Url.AbsolutePath.ToString();
            try
            {
                string host = Request.Url.Host;
                if (!Hosts.TrueForAll(x => host.IndexOf(x) == -1))
                {
                    Match match = Regex.Match(input, @"(.*?)\.(html|htm)", RegexOptions.IgnoreCase);
                    if (match.Success)
                    {

                        string[] source = match.Groups[1].ToString().Trim(new char[] { '/' }).Split(new char[] { '/' });
                        int num = 2;//string.IsNullOrEmpty("GlobalConfig.VirtualPath") ? 2 : 3;
                        num = Area.Contains(source[0].ToLower()) ? 3 : 2;
                        if (Regex.Match(source.FirstOrDefault<string>(), "^[a-zA-Z]{2}$", RegexOptions.IgnoreCase).Success)
                        {
                            if (source.Length > 1)
                            {
                                if (IsNumber((source.GetValue(1) ?? string.Empty).ToString().Split(new char[] { '~' }).LastOrDefault<string>()))
                                {
                                    num = 4;
                                    source[1] = string.Empty;
                                }
                                else
                                {
                                    num = 3;
                                }
                            }
                        }
                        else if (IsNumber(source.FirstOrDefault<string>().Split(new char[] { '~' }).LastOrDefault<string>()))
                        {
                            num = 3;
                            source[0] = string.Empty;
                        }
                        string str = source.LastOrDefault<string>().Split(new char[] { '-' }).LastOrDefault<string>();
                        if (IsNumber(str))
                        {
                            source[source.Length - 1] = (source.Length == num) ? string.Format("index/{0}", str) : str;
                        }
                        List<string> values = new List<string>();
                        foreach (string str8 in source)
                        {
                            if (!string.IsNullOrEmpty(str8))
                            {
                                values.Add(UppercaseWords(str8));
                            }
                        }
                        Context.RewritePath("/" + string.Join("/", values), true);
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Rewrite url
        /// </summary>
        /// <param name="urlbase"></param>
        /// <param name="title"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ReUrl(string urlbase, params string[] param)
        {
            return string.Format(urlbase, param);
        }
        public static string StringAlias(params object[] paramStrings)
        {
            if (paramStrings.Length == 0)
            {
                return null;
            }
            string str = string.Join("-", paramStrings);
            if (str.Length > 200)
            {
                decimal num = 200M;
                List<object> values = new List<object>();
                int charCount = (int)Math.Floor((decimal)(num / paramStrings.Length));
                foreach (object obj2 in paramStrings)
                {
                    values.Add(Summarychar(obj2.ToString(), charCount));
                }
                str = string.Join<object>("-", values);
            }
            bool flag = true;
            string str2 = str.Normalize(NormalizationForm.FormD);
            StringBuilder builder = new StringBuilder();
            foreach (char ch in str2)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                {
                    if (char.IsLetterOrDigit(ch))
                    {
                        flag = true;
                        builder.Append(ch);
                    }
                    else if (flag)
                    {
                        flag = false;
                        builder.Append('-');
                    }
                }
            }
            return builder.Replace('Đ', 'D').Replace('đ', 'd').ToString().Normalize(NormalizationForm.FormD).Trim(new char[] { '-' });
        }

        public static string StgPath(string path)
        {
            return "/stg/" + path;
        }

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes("dacdeptrai"));
            }
            else keyArray = Encoding.UTF8.GetBytes("dacdeptrai");
            var tdes = new TripleDESCryptoServiceProvider
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string toDecrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);
            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes("dacdeptrai"));
            }
            else keyArray = Encoding.UTF8.GetBytes("dacdeptrai");
            var tdes = new TripleDESCryptoServiceProvider
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Encoding.UTF8.GetString(resultArray);
        }
        public static string GetStringSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }
        public static int GetIntSetting(string name)
        {
            int result = 0;
            int.TryParse(ConfigurationManager.AppSettings[name], out result);
            return result;
        }
        public static Uri GetRedirectUri(Uri url, string action)
        {
            var uriBuilder = new UriBuilder(url);
            uriBuilder.Query = null;
            uriBuilder.Fragment = null;
            uriBuilder.Path = action;
            return uriBuilder.Uri;
        }

        /// <summary>
        /// Set param cho url
        /// </summary>
        /// <param name="url">đường url</param>
        /// <param name="param">tên param</param>
        /// <param name="value">giá trị</param>
        /// <returns></returns>
        public static string SetParamForUrl(string url, string param, object value)
        {
            var re = "([?&])" + param + "=.*?(&|$)";
            var separator = url.IndexOf('?') != -1 ? "&" : "?";
            string result;
            if (Regex.Match(url, re).Success)
            {
                result = Regex.Replace(url, @"([?&]" + param + ")=[^?&]+", "$1=" + value);
            }
            else
            {
                result = url + separator + param + "=" + value;
            }
            return result;
        }

        #region  Check Tồn tại 
        /// <summary>
        /// Kiểm tra đối tượng đầu vào cố dữ liệu không
        /// </summary>
        /// <param name="value">giá trị đầu vào</param>
        /// <returns> true : đối tượng không có dữ liệu, False :không có dữ liệu</returns>
        public static bool IsNotEmpty(object value)
        {
            if (value == null)
                return false;
            if (value.GetType() == typeof(string))
            {
                return !string.IsNullOrEmpty(value.ToString());
            }
            else if(value.GetType()==typeof(DateTime?))
            {
                return value.HasValue();
            }
            else if(value.GetType()==typeof(IList))
            {
                var list = (IList)value;
                if (list.Count == 0)
                    return false;
            }
            return true;
        }
        #endregion

        #region Hàm xử lý chuỗi
        /// <summary>
        /// hàm bỏ chữ có dấu
        /// </summary>
        /// <param name="text">Chuỗi đầu vào có dấu</param>
        /// <returns>chuỗi không dấu</returns>
        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
        #endregion

    }
}
