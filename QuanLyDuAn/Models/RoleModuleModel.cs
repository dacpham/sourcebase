﻿using ShopRetailModel.Models;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyDuAn.Models
{
    public class RoleModuleModel
    {
        public List<RoleModule> RoleModules { get; set; }
        public List<RoleModule> Parents { get; set; }
        public RoleModule RoleModule { get; set; }
        public RoleModuleParam SearchParam { get; set; }
        public string Url { get; set; }
    }
}