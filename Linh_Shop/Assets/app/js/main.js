var message = {
    ShowAllMessage: function () {
        var msgsuccess = $("#st-msg-success").val();
        var msgwarning = $("#st-msg-warnings").val()
        var msgerror = $("#st-msg-errors").val()
        var msgnotify = $("#st-msg-notifys").val()
        if (!Utils.isEmpty(msgsuccess)) {
            Utils.setToastSuccess(msgsuccess);
        }
        else {
            if (!Utils.isEmpty(msgerror)) {
                Utils.setToastError(msgerror);
            }
            if (!Utils.isEmpty(msgwarning)) {
                Utils.setToastWarning(msgwarning);
            }
            if (!Utils.isEmpty(msgnotify)) {
                Utils.setToastInfor(msgnotify);
            }
        }
    }
}

$(document).ready(function(){
	

    message.ShowAllMessage();
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VI/all.js#xfbml=1&appId=" + "670077443176137"; // load js for share facebook
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'))
    ;
});
