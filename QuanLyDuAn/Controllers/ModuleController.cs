﻿using QuanLyDuAn.Models;
using System.Linq;
using System.Web.Mvc;
using Common.Dac;
using ShopRetailModel.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Params;
using QuanLyDuAn.Customs.Params;

namespace ShopAdmin.Controllers
{
    public class ModuleController : BaseController
    {
        private string defaultPath = "/module.html";
        public ModuleController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles, IAccount resAccount, IDept resDept, IPosition resPosition, IModule resModule, IRole resRole, IRoleModule resRoleModule, IAccountRole resAccountRole) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles, resAccount, resDept, resPosition, resModule, resRole, resRoleModule, resAccountRole)
        {
        }

        #region List
        public ActionResult Index(int id = 0)
        {
            SetTitle(DACS.T("Danh sách module"));
            var param = DACS.Bind<ModuleParam>(DATA);
            var modules = _resModule.Search(param, Paging);
            var model = new ModuleModel()
            {
                Modules = modules,
                SearchParam = param,
                Url = DACS.ReUrl("/module.html")
            };
            return GetCustResultOrView(new ViewParam()
            {
                ViewName = "Index",
                ViewNameAjax = "Modules",
                Data = model
            });
        }
        public ActionResult Create(int id = 0)
        {
            SetTitle(DACS.T("Tạo module mới"));
            var module = new Module();
            var model = new ModuleModel()
            {
                Module = module,
                SearchParam = new ModuleParam(),
                Url = DACS.ReUrl("/module/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }
        public ActionResult Update(int id = 0)
        {
            SetTitle(DACS.T("Cập nhật module"));
            var module = _resModule.GetById(id);
            if (DACS.IsEmpty(module))
            {
                SetError(DACS.T("module không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var model = new ModuleModel()
            {
                Module = module,
                Url = DACS.ReUrl("/module/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }

        public ActionResult Save()
        {
            var entity = new Module();
            var module = DACS.Bind<Module>(DATA);
            if (module.ID == 0) // Insert
            {
                entity = DACS.BindCreate<Module>(module, CUser.ID);
                //Get max id
                var lastModule = _resModule.GetAll().OrderByDescending(n => n.ID).First() ?? new Module();
                entity.ID = lastModule.ID + 1;
                if (_resModule.Insert(entity))
                {
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                
                entity = _resModule.GetById(module.ID);
                entity = DACS.Bind<Module>(entity, DATA, 0, CUser.ID);
                if (_resModule.Update(entity))
                {
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return RedirectToPath(defaultPath);
        }

        public ActionResult IsDelete(DeleteParam deleteParam)
        {
            var module = _resModule.GetById((int)deleteParam.ID);
            if (Equals(module, null))
            {
                SetWarn(DACS.T("module không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDelete(new DeleteParam
            {
                ID = module.ID,
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/module/delete.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa module"),
                Title = DACS.T("Xác nhận xóa module")
            });
        }

        public ActionResult Delete(DeleteParam deleteParam)
        {
            var module = _resModule.GetById((int)deleteParam.ID);
            if (Equals(module, null))
            {
                SetWarn(DACS.T("module không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            else
            {
                if (_resModule.Delete(module.ID))
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }

        public ActionResult IsDeletes(DeletesParam deletesParam)
        {
            if (!deletesParam.HasID)
            {
                SetWarn(DACS.T("Bạn chưa chọn module cần xóa"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var items = _resModule.GetItems(deletesParam.IDToInts());
            if (!items.Any())
            {
                SetWarn(DACS.T("module không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDeletes(new DeletesParam
            {
                ID = items.Select(x => (long)x.ID).ToArray(),
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/module/deletes.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa {0} module đã chọn?", items.Count),
                Title = DACS.T("Xác nhận xóa module")
            });
        }

        public ActionResult Deletes(DeletesParam deletesParam)
        {
            var modules = _resModule.GetItems(deletesParam.IDToInts());
            if (!modules.Any())
                SetError(DACS.T("module không còn tồn tại"));
            else
            {
                if (_resModule.Deletes(deletesParam.ID) > 0)
                    SetSuccess(DACS.T("Xóa {0} module thành công", modules.Count));
                else
                    SetError(DACS.T("Lỗi: xóa module không thành công"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }
        #endregion
    }
}