﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public static class PrincipalExtensions
    {
        public static string GetName(this IPrincipal user)
        {
            var claimIdentity = (ClaimsIdentity)user.Identity;
            var claimName = claimIdentity.FindFirst(ClaimTypes.GivenName);
            return claimName == null ? string.Empty : claimName.Value;
        }
    }
}
