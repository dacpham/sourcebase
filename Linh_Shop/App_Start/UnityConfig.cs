using ShopRetailCore.Interfaces;
using ShopRetailModel.Interfaces;
using ShopRetailServiceAdmin.Context;
using ShopRetailServiceAdmin.ServiceModels;
using Microsoft.Practices.Unity;
using System.Web.Http;
using System.Web.Mvc;
using Unity.WebApi;

namespace ShopRetail
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            RegisterResolver(container);
            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            DependencyResolver.SetResolver(new ResolverUnityDependency(container));
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static void RegisterResolver(IUnityContainer container)
        {
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<IDbContext, DocProDbContext>();
            container.RegisterType<IDbContextAdmin, DbContextAdmin>();

            container.RegisterType<IUser, UserService>();
            container.RegisterType<ICategory, CategoryService>();
            container.RegisterType<IEvent, EventService>();
            container.RegisterType<IExportDetail, ExportDetailService>();
            container.RegisterType<IExport, ExportService>();
            container.RegisterType<IGuest, GuestService>();
            container.RegisterType<IImportDetail, ImportDetailService>();
            container.RegisterType<IImport, ImportService>();
            container.RegisterType<IOrderDetail, OrderDetailService>();
            container.RegisterType<IOrder, OrderService>();
            container.RegisterType<IProduct, ProductService>();
            container.RegisterType<IProductViewed, ProductViewedService>();
            container.RegisterType<ISliderInfor, SliderInforService>();
            container.RegisterType<IStatus, StatusService>();
            container.RegisterType<IStgFiles, StgFilesService>();
        }
    }
}