﻿using ShopDetail.Models;
using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using Common.Dac;
using ShopDetail.Customs.Enum;
using ShopRetailModel.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Params;
using ShopDetail.Customs.Params;

namespace ShopAdmin.Controllers
{
    public class DeptController : BaseController
    {
        private string defaultPath = "/dept.html";
        public DeptController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles, IAccount resAccount, IDept resDept, IPosition resPosition, IModule resModule, IRole resRole, IRoleModule resRoleModule, IAccountRole resAccountRole) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles, resAccount, resDept, resPosition, resModule, resRole, resRoleModule, resAccountRole)
        {
        }

        #region List
        public ActionResult Index(int id = 0)
        {
            SetTitle(DACS.T("Danh sách phòng ban"));
            var param = DACS.Bind<DeptParam>(DATA);
            param.Parent = id;
            var depts = _resDept.Search(param, Paging);
            var model = new DeptModel()
            {
                Depts = depts,
                SearchParam = param,
                Url = DACS.ReUrl("/dept.html")
            };
            return GetCustResultOrView(new ViewParam()
            {
                ViewName = "Index",
                ViewNameAjax = "Depts",
                Data = model
            });
        }
        public ActionResult Create(int id = 0)
        {
            SetTitle(DACS.T("Tạo phòng ban mới"));
            var dept = new Dept();
            var parents = _resDept.GetListByField("IDChannel", CUser.IDChannel);
            var model = new DeptModel()
            {
                Dept = dept,
                Parents = parents,
                SearchParam = new DeptParam() { Parent = id },
                Url = DACS.ReUrl("/dept/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }
        public ActionResult Update(int id = 0)
        {
            SetTitle(DACS.T("Cập nhật phòng ban"));
            var dept = _resDept.GetById(id);
            if (DACS.IsEmpty(dept))
            {
                SetError(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var parents = _resDept.GetListByField("IDChannel", CUser.IDChannel).Where(n => n.ID != id).ToList();
            var model = new DeptModel()
            {
                Dept = dept,
                Parents = parents,
                Url = DACS.ReUrl("/dept/save.html")
            };
            return GetDialogResultOrView("Update", model);
        }

        public ActionResult Save()
        {
            Dept parent;
            Dept entity;
            var dept = DACS.Bind<Dept>(DATA);
            if (dept.ID == 0) // Insert
            {
                entity = DACS.BindCreate<Dept>(dept, CUser.ID);
                parent = _resDept.GetById(entity.Parent) ?? new Dept();
                entity.Parents = (DACS.IsNotEmpty(parent.Parents) ? parent.Parents + "|" : string.Empty) + entity.Parent;
                if (_resDept.Insert(entity))
                {
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                entity = _resDept.GetById(dept.ID);
                entity = DACS.Bind<Dept>(entity, DATA, 0, CUser.ID);
                parent = _resDept.GetById(entity.Parent) ?? new Dept();
                entity.Parents = (DACS.IsNotEmpty(parent.Parents) ? parent.Parents + "|" : string.Empty) + entity.Parent;
                if (_resDept.Update(entity))
                {
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return RedirectToPath(DACS.ReUrl(DACS.admin + "/dept.html?id={0}", entity.Parent.ToString()));
        }

        public ActionResult IsDelete(DeleteParam deleteParam)
        {
            var dept = _resDept.GetById((int)deleteParam.ID);
            if (Equals(dept, null))
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDelete(new DeleteParam
            {
                ID = dept.ID,
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/dept/delete.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa phòng ban"),
                Title = DACS.T("Xác nhận xóa phòng ban")
            });
        }

        public ActionResult Delete(DeleteParam deleteParam)
        {
            var dept = _resDept.GetById((int)deleteParam.ID);
            if (Equals(dept, null))
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            else
            {
                if (_resDept.Delete(dept.ID))
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }

        public ActionResult IsDeletes(DeletesParam deletesParam)
        {
            if (!deletesParam.HasID)
            {
                SetWarn(DACS.T("Bạn chưa chọn phòng ban cần xóa"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            var items = _resDept.GetItems(deletesParam.IDToInts());
            if (!items.Any())
            {
                SetWarn(DACS.T("Phòng ban không còn tồn tại"));
                return GetResultOrRedirectDefault(defaultPath);
            }
            return GetDialogResultOrViewDeletes(new DeletesParam
            {
                ID = items.Select(x => (long)x.ID).ToArray(),
                RedirectPath = GetRedirectOrDefault(defaultPath),
                Action = DACS.ReUrl("/dept/deletes.html"),
                BackTitle = DACS.T("Quay lại"),
                ConfTitle = DACS.T("Xóa {0} phòng ban đã chọn?", items.Count),
                Title = DACS.T("Xác nhận xóa phòng ban")
            });
        }

        public ActionResult Deletes(DeletesParam deletesParam)
        {
            var depts = _resDept.GetItems(deletesParam.IDToInts());
            if (!depts.Any())
                SetError(DACS.T("Phòng ban không còn tồn tại"));
            else
            {
                if (_resDept.Deletes(deletesParam.ID) > 0)
                    SetSuccess(DACS.T("Xóa {0} phòng ban thành công", depts.Count));
                else
                    SetError(DACS.T("Lỗi: xóa phòng ban không thành công"));
            }
            return GetResultOrRedirectDefault(defaultPath);
        }
        #endregion
    }
}