﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace QuanLyDuAn.Customs.Enum
{
    public static class CusEnum
    {
        public enum TypeGuest
        {
            System = 0,
            Facebook = 1,
            Google = 2,
            Twitter = 3
        }

        /// <summary>
        /// Giới tính
        /// </summary>
        public enum Gender
        {
            [Description("Nam")]
            Male = 1,
            [Description("Nữ")]
            Female = 2
        }
        /// <summary>
        /// Trạng thái tk
        /// </summary>
        public enum AccountStatus
        {
            [Description("Duyệt")]
            Approved = 1,
            [Description("Chưa duyệt")]
            DisApproved = 2
        }
    }
}