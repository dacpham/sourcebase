﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Customs.Params
{
    public class LoginParam
    {
        public string Code { get; set; }
        public string Destination { get; set; }
    }
}