﻿jQuery(document).ready(function () {
    init();
    //.itvideo

   

    function Run(ctrvideo, type) {
        var controlvs = $(ctrvideo);
        var video = controlvs.find('video').get(0).id;
        var maxlst = controlvs.data("maxlist");
        var list = controlvs.data("list");
        var clst = controlvs.data("clst"); // video hiện tại
        if (type == 'next') {
            if (clst == maxlst) // video hiện tại là video cuối 
            {
                clst = 1;
            }
            else {
                clst = clst + 1;
            }
            playvideo(clst, controlvs);
        }
        else if (type == 'prev') {
            if (clst == 1)  // video hiện tại là video cuối
            {
                clst = 1;
            }
            else {
                clst = clst - 1;
            }
            playvideo(clst, controlvs);
        }
        else {
            setplaytoogle(video);
        }
    }
    function setplaytoogle(video) {
        if ($(video).paused) {
            $(video).trigger("play");
            return true;
        }
        else {
            $(video).trigger("play");
            return false;
        }
    }
    function playvideo(clst, ctrvideo)
    {
        var list = ctrvideo.data("list");
        var hitm = ".hitm" + clst;
        var link = $(list).find(hitm).data("link");
        ctrvideo.data("clst", clst);
        setactivelst(hitm, list);
        setplay(ctrvideo, link);
    }
    function setactivelst(item, list) {
        $(list).find('.itvideo').each(function () {
            $(this).removeClass('active');
        })
        $(list).find(item).addClass('active');
    }
    function setplay(ctrvideo, link)
    {
        var video = "#" + ctrvideo.find('video').get(0).id;
        sethiddennextprev(ctrvideo);
        $(video).attr("src", link).trigger("play");
    }
    function addbtnnextprev(ctrvideo)
    {
        var btnext = "<button class='vjs-control vjs-button vjs-button-next' data-ctrvideo='"+ctrvideo+"' type='button' aria-live='polite' title='Next' aria-disabled='false'><i class='fa fa-step-forward' aria-hidden='true'></i></button>";
        var btprev = "<button class='vjs-control vjs-button vjs-button-prev' data-ctrvideo='"+ctrvideo+"'  type='button' aria-live='polite' title='Prev' aria-disabled='false'><i class='fa fa-step-backward' aria-hidden='true'></i></button>";
        $(ctrvideo).find('.vjs-control-bar button.vjs-play-control').before(btprev);
        $(ctrvideo).find('.vjs-control-bar button.vjs-play-control').after(btnext);
        sethiddennextprev($(ctrvideo));
    }
    function sethiddennextprev(ctrvideo)
    {
        var maxlst = ctrvideo.data("maxlist");
        var clst = ctrvideo.data("clst");
        if(maxlst==1)
        {
            $(ctrvideo).find('.vjs-control-bar button.vjs-button-next').addClass("hidden");
            $(ctrvideo).find('.vjs-control-bar button.vjs-button-prev').addClass("hidden");
        }
        else {
            if(clst==1)
            {
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-next').removeClass("hidden");
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-prev').removeClass("hidden");
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-prev').addClass("hidden");
            }
            else if (clst == maxlst) {
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-next').removeClass("hidden");
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-prev').removeClass("hidden");
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-next').addClass("hidden");
            }
            else
            {
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-next').removeClass("hidden");
                $(ctrvideo).find('.vjs-control-bar button.vjs-button-prev').removeClass("hidden");
            }
        }
    }
    function init() {
        videojs('video_1', {
            controls: true,
            controlBar: {
                muteToggle: false
            },
            muted: true,
            plugins: {
                videoJsResolutionSwitcher: {
                    ui: true,
                    default: 'low', // Default resolution [{Number}, 'low', 'high'],
                    dynamicLabel: true // Display dynamic labels or gear symbol
                }
            }
        });
        
        videojs('video_1').videoJsResolutionSwitcher();
        //Call event when audio is ended
        $('#ctrvideo video').on('ended', function (e) {
            Run("#ctrvideo", 'next');
        });
        addbtnnextprev("#ctrvideo");
        $(document).on("click", ".vjs-button-next,.vjs-button-prev", function () {
            var ctrvideo= $(this).data('ctrvideo');
            if ($(this).attr("class").indexOf("vjs-button-prev") > 0) {
                Run(ctrvideo, 'prev');
            }
            else {
                Run(ctrvideo, 'next');
            }
        });
        $(document).on("click", ".itvideo", function () {
            var stt = $(this).data("stt");
            var link = $(this).data("link");
            var ctrvideo = $(this).data("ctrvideo");
            setplay($(ctrvideo), link);
            playvideo(stt, $(ctrvideo));
        });
    }

});
