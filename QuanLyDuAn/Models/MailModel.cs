﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyDuAn.Models
{
    public class MailModel
    {
        public string mailto { get; set; }
        public string body { get; set; }
        public string subject { get; set; }
        public string displayname { get; set; }
    }
}