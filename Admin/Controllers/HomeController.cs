﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using ShopRetailModel.Interfaces;
using ShopAdmin.Controllers;
using ShopRetailModel.Models;

namespace ShopDetail.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles, IAccount resAccount, IDept resDept, IPosition resPosition, IModule resModule, IRole resRole, IRoleModule resRoleModule, IAccountRole resAccountRole) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles, resAccount, resDept, resPosition, resModule, resRole, resRoleModule, resAccountRole)
        {
        }
        //[AuthorizeUser(Quyen="admin")]
        public ActionResult Index(int id = 0)
        {
            if (id == 0)
                return GetCustResultOrView("Index");
            return GetCustResultOrView("ChiTiet");
        }
    }
}