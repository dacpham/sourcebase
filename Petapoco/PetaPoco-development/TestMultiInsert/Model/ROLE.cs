﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMultiInsert.Models
{
    [TableName("ROLE")]
    [PrimaryKey("ID", AutoIncrement = true)]
    public class ROLE
    {
        [Column]
        public int ID { get; set; }

        [Column]
        public int IDChannel { get; set; }

        [Column]
        public int Parent { get; set; }
        [Column]
        public string Name { get; set; }

        [Column]
        public string Parents { get; set; }

        [Column]
        public string SEARCHMETA { get; set; }

    }
}
