﻿using ShopRetailModel.Models;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Models
{
    public class PositionModel
    {
        public List<Position> Positions { get; set; }
        public List<Position> Parents { get; set; }
        public Position Position { get; set; }
        public PositionParam SearchParam { get; set; }
        public string Url { get; set; }
    }
}