﻿using ShopRetailCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using PetaPoco;
using ShopRetailServiceAdmin.Interfaces;

namespace ShopRetailCore.Context
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbContext _dbContext;

        public UnitOfWork(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void SetDataContext(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IDbContext DataContext
        {
            get
            {
                return _dbContext;
            }
        }

        public void BeginTransaction()
        {
            _dbContext.DbInstance.BeginTransaction();
        }

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            _dbContext.DbInstance.IsolationLevel = isolationLevel;
            _dbContext.DbInstance.BeginTransaction();
        }

        public ITransaction GetTransaction()
        {
            return _dbContext.DbInstance.GetTransaction();
        }

        public void CommitTransaction()
        {
            _dbContext.DbInstance.CompleteTransaction();
        }

        public void AbortTransaction()
        {
            _dbContext.DbInstance.AbortTransaction();
        }

        public IRepository<T> GetDbSet<T>() where T : class, new()
        {
            return new Repository<T>(_dbContext, this);
        }
    }
}
