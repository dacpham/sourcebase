﻿using ShopRetailModel.Models;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Models
{
    public class DeptModel
    {
        public List<Dept> Depts { get; set; }
        public List<Dept> Parents { get; set; }
        public Dept Dept { get; set; }
        public DeptParam SearchParam { get; set; }
        public string Url { get; set; }
    }
}