﻿
using Common.Dac;
using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopRetail.Models
{
    public class ContentCategroryModel
    {
        public Category CategoryRoot { get; set; }
        public List<Category> Categories { get; set; }
        public List<ProductViewModel> ContentViews { get; set; }
        public List<Product> Contents { get; set; }
        public void GetContentDetail()
        {
            ContentViews = new List<ProductViewModel>();
            foreach (var item in Contents)
            {
                ContentViews.Add(GetContentDetail(item, Categories));
            }
        }
        public ProductViewModel GetContentDetail(Product content , List<Category> categories)
        {
            var oContentView = new ProductViewModel();
            oContentView = DACS.CopyTo<ProductViewModel>(content);
            var category = categories.Where(t=>t.ID==content.IDCategory).FirstOrDefault();
            if(!DACS.IsEmpty(category))
            {
                oContentView.CategoryName = category.Name;
            }
            return oContentView;
        }
    }
}