﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public  class OptionModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public long Parent { get; set; }
    }
}
