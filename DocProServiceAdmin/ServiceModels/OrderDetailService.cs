﻿using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopRetailCore.Interfaces;
using ShopRetailCore.Context;

namespace ShopRetailServiceAdmin.ServiceModels
{
    public class OrderDetailService : Repository<OrderDetail>, IOrderDetail
    {
        public OrderDetailService(IDbContextAdmin dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {

        }
    }
}
