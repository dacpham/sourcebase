﻿using Common.Dac;
using Common.Enum;
using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Common
{
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        public Imodule[] Imodules;
        public AuthorizeUserAttribute()
        {

        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var crRequest = DACS.Base64Encode(filterContext.HttpContext.Request.Url.ToString());
            string loginUrl = string.Format("/login/account/login.html?RedirectPath={0}", crRequest);
            if (object.Equals(filterContext, null))
                throw new ArgumentException("filtercontext");
            var c = HttpContext.Current.User.Identity as ClaimsIdentity;
            if (!c.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult(loginUrl);
            }
            else
            {
                Account entity = DacAuthen.GetCurrentUser;
                if (object.Equals(entity, null) || entity.ID ==0)
                    filterContext.Result = new RedirectResult(loginUrl);
                else if(Imodules.Count()>0) // Check quyền
                {
                    
                }
            }
        }
    }
}
