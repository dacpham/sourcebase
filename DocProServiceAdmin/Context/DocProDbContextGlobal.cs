﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailServiceAdmin.Context
{
    public class DocProDbContextGlobal : DocProDbContext
    {
        public DocProDbContextGlobal(string connectionNameString)
        {
            SetDatabase(connectionNameString);
        }
    }
}
