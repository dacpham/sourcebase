﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMultiInsert.Models
{
    [TableName("STGDOC")]
    [PrimaryKey("ID", AutoIncrement = true)]
    public class STGDOC
    {
        [PetaPoco.Column]
        public int ID { get; set; }
        [PetaPoco.Column]
        public int RANK { get; set; }

        [PetaPoco.Column]
        public string NAME { get; set; }

        [PetaPoco.Column]
        public string DESCRIBE { get; set; }

        [PetaPoco.Column]
        public string SEARCHMETA { get; set; }


    }

    [TableName("Account")]
    [PrimaryKey("ID", AutoIncrement = true)]
    public class Account
    {
        [PetaPoco.Column]
        public int ID { get; set; }
        [PetaPoco.Column]
        public string NAME { get; set; }

        [PetaPoco.Column]
        public string SEARCHMETA { get; set; }


    }
}
