﻿using ShopRetailModel.Models;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Models
{
    public class AccountModel : Account
    {
        public List<Account> Accounts { get; set; }
        public Account Account { get; set; }
        public AccountParam SearchParam { get; set; }
        public List<Dept> Depts { get; set; }
        public List<Position> Positions { get; set; }
        public Dictionary<int, string> DicGender { get; set; }
        public Dictionary<int, string> DicStatus { get; set; }
        public string Url { get; set; }
        public List<Role> Roles { get; set; }
        public List<AccountRole> AccountRoles { get; set; }
    }
}