﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Models.QLDA
{
    [TableName("rojectDetailFile")]



    [PrimaryKey("ID", AutoIncrement = false)]




    public partial class ProjectDetailFile
    {



        [Column]
        public int ID { get; set; }





        [Column]
        public int IDProjectDetailFolder { get; set; }





        [Column]
        public string FileName { get; set; }





        [Column]
        public string Path { get; set; }





        [Column]
        public long Size { get; set; }





        [Column]
        public string Extention { get; set; }





        [Column]
        public int CreatedBy { get; set; }





        [Column]
        public DateTime Created { get; set; }





        [Column]
        public DateTime? Updated { get; set; }





        [Column]
        public int? UpdatedBy { get; set; }



    }
}
