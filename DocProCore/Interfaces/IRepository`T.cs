﻿using Common.Extensions;
using PetaPoco;
using ShopRetailCore.Interfaces;
using ShopRetailCore.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailServiceAdmin.Interfaces
{
    public interface IRepository<T> : IRepository where T : class, new()
    {
        /// <summary>
        /// Xóa bản ghi bằng cách thay đổi giá trị trường IsDeleted, dữ liệu vật lý vẫn còn
        /// </summary>
        /// <param name="entity"></param>
        int DeleteFlag(T entity);

        int DeleteFlag(Sql sqlQuery);

        int DeleteFlags(params object[] ids);

        int Deletes(string field, params object[] fields);
        int Deletes(params object[] ids);
        /// <summary>
        /// Xóa một bản ghi vật lý
        /// </summary>
        /// <param name="entity"></param>
        bool Delete(object id);

        int Delete(Sql sqlQuery);
        /// <summary>
        /// Lấy dữ liệu đối tượng theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);

        T GetById(long id);

        T GetById(Guid id);

        T GetById(string id);

        /// <summary>
        /// Lấy một bản ghi theo điều kiện truyền vào
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        T GetOne(Sql sqlQuery);

        T GetOne(object primaryKey);

        T GetOne(string sql, params object[] args);

        /// <summary>
        /// Lấy tất cả danh sách bản ghi của table
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();

        /// <summary>
        /// lấy về danh sách dữ liệu theo query truyền vào
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        List<T> GetItems(Sql sqlQuery);

        List<T> GetItems(string sql, params object[] args);

        List<T> GetItems(params object[] ids);

        Page<T> Page(Sql sqlQuery, int page, int rowLimit);

        string GetTableName();
        /// <summary>
        /// Cập nhật thông tin bản ghi
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        int Update(string sql, params object[] args);

        int Update(Sql sqlQuery);

        bool Update(T poco, params string[] columns);
        bool Updates(List<T> pocos, params string[] columns);

        /// <summary>
        /// Insert một bản ghi
        /// </summary>
        /// <param name="poco"></param>
        bool Insert(T poco);

        object Insert(string tableName, T poco);

        object Insert(string tableName, string primaryKeyName, object poco, bool autoIncrement = true);
        bool Inserts(List<T> pocos);

        void Save(string tableName, string primaryKey, T poco);

        void Save(T poco);

        bool Exists(object primaryKey);

        //bool Exists(string sqlCondition, params object[] args);
        bool Exists(string field, object value);

        bool Exists(string field, object value, out T entity);
        bool Exists(Sql sql, out T entity);

        /// <summary>
        /// Kiểm tra Name đã tồn tại trong trường hợp cập nhật
        /// </summary>
        /// <param name="name">Tên</param>
        /// <param name="id">Mã của bản ghi cần cập nhật</param>
        /// <returns></returns>
        bool ExistsNameUpdate(string name, object id);
        List<T> GetListByField(string field, object value);
        List<T> GetListByFields(string field, params object[] values);

        /// <summary>
        /// Lay danh sach cac phan tu co ID khac ids
        /// </summary>
        /// <param name="ids">danh sach cac ID loai bo</param>
        /// <returns></returns>
        IEnumerable<T> GetWithoutItems(params object[] ids);
        /// <summary>
        /// Lay danh sach cac phan tu co Filed khong thuoc trong ids
        /// </summary>
        /// <param name="ids">danh sach cac Field loai bo</param>
        /// <returns></returns>
        IEnumerable<T> GetListByFieldWithoutItems(string field, params object[] fields);
        List<T> Search(CommonParam param, Pagination paging);
        List<T> Search(CommonParam param);
        T GetFirstByField(string field, object value);
        T GetFirstByFields(string field, params object[] value);

    }
}
