﻿using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopRetailCore.Interfaces;
using PetaPoco;
using Common.Dac;
using ShopRetailCore.Context;

namespace ShopRetailServiceAdmin.ServiceModels
{
    public class UserService : Repository<User>, IUser
    {
        public UserService(IDbContextAdmin dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {
            
        }

        public bool ValidateLogin(string username, string password, out User user)
        {
            var pa = DACS.Encrypt(password, true);
            Sql sql = Sql.Builder.From(TableName).
                Where("( UserName = @0 Or Email = @1 and PassWord = @2 and IsActive = 1 )", username, username, DACS.Encrypt(password, true));
            user = GetOne(sql);
            return !DACS.IsEmpty(user);
        }
    }
}
