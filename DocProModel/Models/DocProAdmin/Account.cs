using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poco = PetaPoco;
namespace ShopRetailModel.Models
{
    [Poco.TableName("Account")]
    [Poco.PrimaryKey("ID", AutoIncrement = true)]
    [Poco.ExplicitColumns]
    public partial class Account :CreateUpdate
    {
        [Key]
        [Poco.Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ID { get; set; } // int    
         
        [Poco.Column]
        [Display(Name = "IDChannel")]
  
        public int IDChannel { get; set; } // int 

        [Poco.Column]
        [Display(Name = "IDIDDept")]
        public int IDDept { get; set; } // int 


        [Poco.Column]
        [Display(Name = "IDPosition")]
        public int IDPosition { get; set; } // int
        
        [Poco.Column]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Username")]
        public string Username { get; set; } // varchar(30)
        [Poco.Column]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "User Password")]
        public string Password { get; set; } // varchar(256)
        [Poco.Column]
        [MaxLength(120)]
        [StringLength(120)]
        [Display(Name = "Email")]
        public string Email { get; set; } // varchar(120)
        [Poco.Column]
        [MaxLength(120)]
        [StringLength(120)]
        [Display(Name = "Name")]
        public string Name { get; set; } // varchar(120)
        [Poco.Column]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; } // varchar(30)
        [Poco.Column]
        [MaxLength(300)]
        [StringLength(300)]
        [Display(Name = "Address Text")]
        public string Address { get; set; } // nvarchar(300)
        [Poco.Column]
        [MaxLength(1000)]
        [StringLength(1000)]
        [Display(Name = "Avatar")]
        public string Avatar { get; set; } // nvarchar(1000)
        [Poco.Column]
        [Display(Name = "Last Login")]
        public DateTime? LogedIn { get; set; } // datetime
        //[Poco.Column]
        //[Display(Name = "Is Locked")]
        //public bool? IsLocked { get; set; } // bit
        [Poco.Column]
        [Display(Name = "Is Admin")]
        public bool IsAdmin { get; set; } // bit 
        [Poco.Column]
        [Display(Name = "Is Online ")]
        public bool IsOnline { get; set; } // bit 
        [Poco.Column]
        [Display(Name = "Is IsCtv ")]
        public bool IsCtv { get; set; } // bit  
      
        [Poco.Column]
        [Display(Name = "Gender")]
        public int Gender { get; set; } // int 


        [Poco.Column]
        [Display(Name = "Status")]
        public int Status { get; set; } // int

    }
}
