﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyDuAn.Customs.Entities
{
    public class BlockEntity
    {
        public dynamic Data { get; set; }
        public string Path { get; set; }
        public string Partial { get; set; }
        public string CssBlock { get; set; }
    }
}