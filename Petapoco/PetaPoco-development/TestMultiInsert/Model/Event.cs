﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMultiInsert.Models
{
    [TableName("Event")]
    [PrimaryKey("ID", AutoIncrement = true)]
    public class Event
    {
        [Column]
        public int ID { get; set; }

        [Column]
        public string Name { get; set; }

        [Column]
        public int CreatedBy { get; set; }

        [Column]
        public DateTime Created { get; set; }

        [Column]
        public int UpdatedBy { get; set; }

        [Column]
        public DateTime Updated { get; set; }

    }
}
