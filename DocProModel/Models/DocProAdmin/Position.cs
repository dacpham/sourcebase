using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poco = PetaPoco;
namespace ShopRetailModel.Models
{
    [Poco.TableName("Position")]
    [Poco.PrimaryKey("ID", AutoIncrement = true)]
    [Poco.ExplicitColumns]
    public partial class Position : CreateUpdate
    {
        [Key]
        [Poco.Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ID { get; set; } // int    

        [Poco.Column]
        [Display(Name = "IDChannel")]
        public int IDChannel { get; set; } // int 

        [Poco.Column]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Poco.Column]
        [Display(Name = "Describe")]
        public string Describe { get; set; }
        [Poco.Column]
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Poco.Column]
        [Display(Name = "Parent")]
        public int Parent { get; set; }

        [Poco.Column]
        [Display(Name = "Parents")]
        public string Parents { get; set; }
        [Poco.Column]
        [Display(Name = "UserCount")]
        public string UserCount { get; set; }

        [Poco.Column]
        [Display(Name = "SearchMeta")]
        public string SearchMeta { get; set; }

    }
}
