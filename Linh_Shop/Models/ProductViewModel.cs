﻿
using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopRetail.Models
{
    public class ProductViewModel:Product
    {
        public string CategoryName { get; set; }
    }
}