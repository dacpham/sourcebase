﻿using System.Web.Optimization;

namespace ShopRetail
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Bootstrap
            bundles.Add(new StyleBundle("~/Assets/bootstrap/css.css").Include(
                "~/Assets/bootstrap/css/bootstrap.min.css"
            ));
            bundles.Add(new ScriptBundle("~/Assets/bootstrap/js.js").Include(
                "~/Assets/bootstrap/js/bootstrap.js",
                "~/Assets/bootstrap/js/bootstrap.switch.js"
            ));

            //JQuery
            bundles.Add(new StyleBundle("~/Assets/jquery/css.css").Include(
                "~/Assets/jquery/css/jquery.ui.css",
                "~/Assets/jquery/css/jquery.animate.css",
                "~/Assets/jquery/css/jquery.rating.css",
                "~/Assets/jquery/css/jquery.cscrollbar.css",
                "~/Assets/jquery/css/jquery.owlCarousel.css",
                "~/Assets/jquery/css/jquery.owlTransitions.css",
                "~/Assets/jquery/css/jquery.datetimepicker.css",
                "~/Assets/jquery/css/jquery.select2.css"
                ));
            bundles.Add(new ScriptBundle("~/Assets/jquery/js.js").Include(
                "~/Assets/jquery/js/jquery.js",
                "~/Assets/jquery/js/jquery.ui.js",
                "~/Assets/jquery/js/jquery.serializejson.js",
                "~/Assets/jquery/js/jquery.rating.js",
                "~/Assets/jquery/js/jquery.datetimepicker.js",
                "~/Assets/jquery/js/jquery.cscrollbar.js",
                "~/Assets/jquery/js/jquery.mousewheel.js",
                "~/Assets/jquery/js/jquery.owlCarousel.js",
                "~/Assets/jquery/js/jquery.select2.js",
                "~/Assets/jquery/js/jquery.nice.select.js"
            ));

            //App - all theme
            bundles.Add(new StyleBundle("~/Assets/app/css.css").Include(
                "~/Assets/app/css/font-awesome.min.css",
                "~/Assets/app/css/titatoggle-dist-min.css"
             ));

            bundles.Add(new ScriptBundle("~/Assets/app/js.js").Include(
                  "~/Assets/app/js/jquery.owlCarousel.js",
                "~/Assets/app/js/toastr.min.js",
                "~/Assets/app/js/utils.js",
                "~/Assets/app/js/pages.js",
                "~/Assets/app/js/main.js",
                "~/Assets/app/js/uploader.js"
            ));

            //App - theme 1
            bundles.Add(new StyleBundle("~/Assets/app/theme1css.css").Include(
                "~/Assets/app/css/account.css",
                "~/Assets/app/css/form-elements.css",
                "~/Assets/app/css/custom.css",
                "~/Assets/app/css/prettyPhoto.css",
                "~/Assets/app/css/price-range.css",
                "~/Assets/app/css/animate.css",
                "~/Assets/app/css/main.css",
                "~/Assets/app/css/responsive.css",
                "~/Assets/app/css/jquery.owlCarousel.css",
                "~/Assets/app/css/jquery.owlTheme.css",
                "~/Assets/app/css/style.css"
             ));

            bundles.Add(new ScriptBundle("~/Assets/app/theme1js.js").Include(

                "~/Assets/app/js/maintheme1.js",
                "~/Assets/app/js/account.js",
                "~/Assets/uploadfile/uploader.js",
                "~/Assets/app/js/jquery.scrollUp.min.js",
                "~/Assets/app/js/price-range.js",
                "~/Assets/app/js/jquery.prettyPhoto.js",
                "~/Assets/app/js/tagdiv_theme.js",
                "~/Assets/app/js/main.js"));

            //compressor
            //    BundleTable.EnableOptimizations = GlobalConfig.WebOptimization;
        }
    }
}